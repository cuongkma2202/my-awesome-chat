// extras
function searchConversation() {
  $("#input-search-conversation").bind("keypress", function(element) {
    if (element.which === 13) {
      let keyword = $("#input-search-conversation").val();
      console.log(keyword);
      let regexKeyword = new RegExp(/^[\s0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/);

      if (!keyword.length) {
        alertify.notify("Please enter conversation name !.", "error", 7);
        return false;
      }

      if (!regexKeyword.test(keyword)) {
        alertify.notify("Invalid keyword.", "error", 7);
        return false;
      }

      $.get(`/conversation/search/${keyword}`, function(data) {
        console.log(data);
        $("#search-results").find("ul").html(data);
        $("#search-results").css("display", "block");

        $(document).click(function() {
          $("#search-results").css("display", "none");
        });

        $("#search-results").find("li").bind("click", function() {
          let dataChat = $(this).data("chat");
          $("ul.people").find(`a[href="#uid_${dataChat}"]`).click();
        });
      });
    }
  });
}

function readMorePersonalChat() {
  $("#link-read-more-user-chat").bind("click", function() {
    let skipPersonalNumber = $("#user-chat").find("li").length;

    $("#link-read-more-user-chat").css("display", "none");
    $(".read-more-user-chat-loader").css("display", "inline-block");

    $.get(`/message/read-more-personal-chat?skipPersonalNumber=${skipPersonalNumber}`, function(data) {
      if (data.leftSideData.trim() === "") {
          alertify.notify("All conversations showed", "error", 7);
        $("#link-read-more-user-chat").css("display", "inline-block");
        $(".read-more-user-chat-loader").css("display", "none");

        return false;
      }

      // Step 01: handle leftSide
      $("#user-chat").find("ul").append(data.leftSideData);

      // Step 02: handle scroll left
      resizeNineScrollLeft();
      nineScrollLeft();

      // Step 03: handle rightSide
      $("#screen-chat").append(data.rightSideData);

      // Step 04: call Screen chat
      changeScreenChat();

      // Step 05: convert emoji
      convertEmoji();

      // Step 06: handle imageModal
      $("body").append(data.imageModalData);

      // Step 07: call gridPhotos
      gridPhotos(5);

      // Step 08: handle attachment Modal
      $("body").append(data.attachmentModalData);

      // Step 09: check online
      socket.emit("check-status");

      // Step 10: remove loading
      $("#link-read-more-user-chat").css("display", "inline-block");
      $(".read-more-user-chat-loader").css("display", "none");

      // step 11: call readmoreMessage
      readMoreMessage();

      // extras
      zoomImageChat();
    });
  });
}

function readMoreGroupChat() {
  $("#link-read-more-group-chat").bind("click", function() {
    let skipGroupNumber = $("#group-chat").find("li").length;

    $("#link-read-more-group-chat").css("display", "none");
    $(".read-more-group-chat-loader").css("display", "inline-block");

    $.get(`/message/read-more-group-chat?skipGroupNumber=${skipGroupNumber}`, function(data) {
      if (data.leftSideData.trim() === "") {
        alertify.notify("All conversations showed", "error", 7);
        $("#link-read-more-group-chat").css("display", "inline-block");
        $(".read-more-group-chat-loader").css("display", "none");
        return false;
      }

      // Step 01: handle leftSide
      $("#group-chat").find("ul").append(data.leftSideData);

      // Step 02: handle scroll left
      resizeNineScrollLeft();
      nineScrollLeft();

      // Step 03: handle rightSide
      $("#screen-chat").append(data.rightSideData);

      // Step 04: call Screen chat
      changeScreenChat();

      // Step 05: convert emoji
      convertEmoji();

      // Step 06: handle imageModal
      $("body").append(data.imageModalData);

      // Step 07: call gridPhotos
      gridPhotos(5);

      // Step 08: handle attachment Modal
      $("body").append(data.attachmentModalData);

      // Step 09: check online
      socket.emit("check-status");

      // Step 10: remove loading
      $("#link-read-more-group-chat").css("display", "inline-block");
      $(".read-more-group-chat-loader").css("display", "none");

      // step 11: call readmoreMessage
      readMoreMessage();


      zoomImageChat();
      $("body").append(data.membersModalData);
      userTalk();
    });
  });
}

$(document).ready(function() {
  searchConversation();
  readMorePersonalChat();
  readMoreGroupChat();
});
