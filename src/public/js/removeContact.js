function removeContact() {
    $(".user-remove-contact").unbind("click").on("click", function() {
        // console.log("check");
        let targetId = $(this).data("uid");
        let username = $(this).parent().find(`div.user-name p`).text();
        // console.log(username);
        // $("#contacts").find(`ul li[data-uid = ${targetId}]`).remove();
        // decreaseNumberNotifContact("count-contacts");
        Swal.fire({
            title: `Are you sure want to remove ${username} as your friend`,
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, Update it!"
        }).then((result) => {
            if (!result.value) {
                return false;
            }
            $.ajax({
                url: "/contact/remove-contact",
                type: "delete",
                data: { uid: targetId },
                success: function(data) {
                    if (data.success) {

                        $("#contacts").find(`ul li[data-uid = ${targetId}]`).remove();
                        decreaseNumberNotifContact("count-contacts");
                        // feature chat coming , delete chatbox cuming soon
                        socket.emit("remove-contact", {
                            contactId: targetId
                        });
                        // handle chat after remove contact
                        // step special => check active
                        let checkActive = $("#all-chat").find(`li[data-chat = ${targetId}]`).hasClass("active");

                        // step 1: remove left side
                        $("#all-chat").find(`ul a[href="#uid_${targetId}"]`).remove();
                        $("#user-chat").find(`ul a[href="#uid_${targetId}"]`).remove();
                        // step 2: remove right side
                        $("#screen-chat").find(`div#to_${targetId}`).remove();
                        // step 3: remove image modal

                        $("body").find(`div#imagesModal_${targetId}`).remove();
                        // step 4: remove attachment modal
                        $("body").find(`div#attachmentsModal_${targetId}`).remove();
                        // step 5: focus first conversation // step 5: focus first conversation
                        if(checkActive){
                            $("ul.people").find("a")[0].click();
                        }

                    }
                }
            });
        });
    });
}
socket.on("response-remove-contact", function(user) {
    $("#contacts").find(`ul li[data-uid = ${user.id}]`).remove();
    decreaseNumberNotifContact("count-contacts");


        // handle chat after remove contact
          // step special => check active
        let checkActive = $("#all-chat").find(`li[data-chat = ${user.id}]`).hasClass("active");
        // step 1: remove left side
        $("#all-chat").find(`ul a[href="#uid_${user.id}"]`).remove();
        $("#user-chat").find(`ul a[href="#uid_${user.id}"]`).remove();
        // step 2: remove right side
        $("#screen-chat").find(`div#to_${user.id}`).remove();
        // step 3: remove image modal

        $("body").find(`div#imagesModal_${user.id}`).remove();
        // step 4: remove attachment modal
        $("body").find(`div#attachmentsModal_${user.id}`).remove();
         // step 5: focus first conversation
        if(checkActive){
            $("ul.people").find("a")[0].click();
        }

});

$(document).ready(function() {
    removeContact();
})