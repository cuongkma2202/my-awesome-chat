import { readMoreService, markAllAsReadService } from "../services/notificationService.js"
const readMore = async (req, res) => {
  try {
    // get skip number from query
    let skipNumberNotification = +(req.query.skipNumber);
    // console.log("hello");
    // get moreItem
    let newNotification = await readMoreService(req.user._id, skipNumberNotification);
    return res.status(200).send(newNotification);
  } catch (error) {
    return res.status(500).send(error)
  }
}
const markAllAsRead = async (req, res) => {
  // console.log(req.body.targetUsers);
  try {
    let mark = await markAllAsReadService(req.user._id, req.body.targetUsers);
    // console.log(mark);
    return res.status(200).send(mark);
  } catch (error) {
    return res.status(500).send(error)
  }
}
export { readMore, markAllAsRead }