import { check } from "express-validator";
import { transValidation } from "../../lang/vi.js";

const userValidation = [
  check("gender", transValidation.update_gender).optional().isIn(["male", "female"]),
  check("username", transValidation.update_username).optional().isLength({ min: 3, max: 15 }).matches(/^[\s0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/),
  check("address", transValidation.update_address).optional().isLength({ min: 3, max: 30 }),
  check("phone", transValidation.update_phone).optional().matches(/^(0)[0-9]{9,10}$/)
];
const updatePassword = [
  check("currentPassword", transValidation.user_current_password_error).isLength({ min: 8 }).matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/),
  check("newPassword", transValidation.user_current_password_error).isLength({ min: 8 }).matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/),
  check("confirmNewPassword", transValidation.user_confirm_password_error).custom((value, { req }) => {
    return value === req.body.newPassword;
  })
]

export { userValidation, updatePassword }