const socket = io();

function nineScrollLeft() {
    $(".left").niceScroll({
        smoothscroll: true,
        horizrailenabled: false,
        cursorcolor: "#ECECEC",
        cursorwidth: "7px",
        scrollspeed: 50,
    });
}
function resizeNineScrollLeft(){
    $(".left").niceScroll().resize();
}

function flashMasterNotify() {
    let notify = $(".master-success-message").text();
    if (notify.length) {
        alertify.notify(notify, "success", 7);
    }
}

function nineScrollRight(divId) {
    $(`.right .chat[data-chat=${divId}]`).niceScroll({
        smoothscroll: true,
        horizrailenabled: false,
        cursorcolor: "#ECECEC",
        cursorwidth: "7px",
        scrollspeed: 50,
    });
    $(`.right .chat[data-chat=${divId}]`).scrollTop($(`.right .chat[data-chat=${divId}]`)[0].scrollHeight);
}

function enableEmojioneArea(divId) {
    $(`#write-chat-${divId}`).emojioneArea({
        standalone: false,
        pickerPosition: "top",
        filtersPosition: "bottom",
        tones: false,
        autocomplete: false,
        inline: true,
        hidePickerOnBlur: true,
        search: false,
        shortnames: false,
        events: {
            keyup: function(editor, event) {
                $(`#write-chat-${divId}`).val(this.getText());




            },
            click: function(){
                textAndEmojiChat(divId);
                typingOn(divId);


            },
            blur: function(){
                typingOff(divId);
            }
        },
    });
    $(".icon-chat").bind("click", function(event) {
        event.preventDefault();
        $(".emojionearea-button").click();
        $(".emojionearea-editor").focus();
    });
}

function spinLoaded() {
    $(".master-loader").css("display", "none");
}

function spinLoading() {
    $(".master-loader").css("display", "block");
}

function ajaxLoading() {
    $(document)
        .ajaxStart(function() {
            spinLoading();
        })
        .ajaxStop(function() {
            spinLoaded();
        });
}

function showModalContacts() {
    $("#show-modal-contacts").click(function() {
        $(this).find(".noti_contact_counter").fadeOut("slow");
    });
}

function configNotification() {
    $("#noti_Button").click(function() {
        $("#notifications").fadeToggle("fast", "linear");
        $(".noti_counter").fadeOut("slow");
        return false;
    });
    $(".main-content").click(function() {
        $("#notifications").fadeOut("fast", "linear");
    });
}

function gridPhotos(layoutNumber) {
    $(".show-images").unbind("click").on("click", function(){
        let href = $(this).attr("href");
        let modalImagesId = href.replace("#","").trim();

        let originDataImages = $(`#${modalImagesId}`).find("div.modal-body").html();




        let countRows = Math.ceil(
            $(`#${modalImagesId}`).find("div.all-images>img").length / layoutNumber
        );
        let layoutStr = new Array(countRows).fill(layoutNumber).join("");
        $(`#${modalImagesId}`)
            .find("div.all-images")
            .photosetGrid({
                highresLinks: true,
                rel: "withhearts-gallery",
                gutter: "2px",
                layout: layoutStr,
                onComplete: function() {
                    $(`#${modalImagesId}`).find(".all-images").css({
                        visibility: "visible",
                    });
                    $(`#${modalImagesId}`).find(".all-images a").colorbox({
                        photo: true,
                        scalePhotos: true,
                        maxHeight: "90%",
                        maxWidth: "90%",
                    });
                },
            });
            // catch event  close modal https://stackoverflow.com/questions/12319171/how-to-handle-the-modal-closing-event-in-twitter-bootstrap/
            $(`#${modalImagesId}`).on('hidden.bs.modal', function () {
                $(this).find("div.modal-body").html(originDataImages);
              })

    })

}

function changeTypeChat() {
    $("#select-type-chat").bind("change", function() {
        let optionSelected = $("option:selected", this);
        optionSelected.tab("show");
        if (this.value === "user-chat") {
            $(".create-group-chat").hide();
        } else {
            $(".create-group-chat").show();
        }

    });


}

function changeScreenChat() {
    $(".room-chat").unbind("click").on("click", function() {
        // scroll rightSide
        let divId = $(this).find("li").data("chat");

        $(".person").removeClass("active");
        $(`.person[data-chat=${divId}]`).addClass("active");


        $(this).tab("show");



        nineScrollRight(divId)
        enableEmojioneArea(divId);

        imageChat(divId);
        attachmentChat(divId);
        videoChat(divId);
    })

};
function convertEmoji(){
    $(".convert-emoji").each(function() {
        var original = $(this).html();
        // use .shortnameToImage if only converting shortnames (for slightly better performance)
        var converted = emojione.toImage(original);
        $(this).html(converted);
    });
}
function bufferToBase64(buffer){
    return btoa(
      new Uint8Array(buffer)
        .reduce((data, byte) => data + String.fromCharCode(byte), '')
    );
  }
function zoomImageChat() {
    $(".show-image-chat").unbind("click").on("click", function() {
      $("#img-chat-modal").css("display", "block");
      $("#img-chat-modal-content").attr("src", $(this)[0].src);

      $("#img-chat-modal").on("click", function() {
        $(this).css("display", "none");
      });
    });
  }

  function userTalk() {
    $(".user-talk").unbind("click").on("click", function() {
      let dataChat = $(this).data("uid");
      $("ul.people").find(`a[href="#uid_${dataChat}"]`).click();
      $(this).closest("div.modal").modal("hide");
    });
  }

  function notYetConversations() {
    if(!$("ul.people").find("a").length) {
      Swal.fire({
        title: "Bạn chưa có bạn bè? Hãy tìm kiếm bạn bè để trò chuyện!",
        type: "info",
        showCancelButton: false,
        confirmButtonColor: "#2ECC71",
        confirmButtonText: "Xác nhận",
      }).then((result) => {
        $("#contactsModal").modal("show");
      });
    }
  }


$(document).ready(function() {
    // Hide số thông báo trên đầu icon mở modal contact
    showModalContacts();

    // Bật tắt popup notification
    configNotification();

    // Config scroll left
    resizeNineScrollLeft();
    nineScrollLeft();
    // nineScrollRight();

    // Icon loading khi chạy ajax
    ajaxLoading();

    gridPhotos(5);

    // Thêm người dùng vào danh sách liệt kê trước khi tạo nhóm trò chuyện
    // Flash message master screen
    flashMasterNotify();
    // change type chat
    changeTypeChat();
    // change screen chat
    changeScreenChat();
    if($("ul.people").find("a").length){
        $("ul.people").find("a")[0].click();
    }

    // convert unicode to img emoji
    convertEmoji();

    $('#video-chat-group').bind("click", function(){
        alertify.notify("feature coming soon","error",5);
    });
    zoomImageChat();
    userTalk();
    notYetConversations();

});