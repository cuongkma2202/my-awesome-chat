function readMoreMessage(){
  $(".right .chat").unbind("scroll").on("scroll", function(){
    // get the first message
    let firstMessage = $(this).find(".bubble:first");

    // get position of first message
    let currentOffset = firstMessage.offset().top - $(this).scrollTop();
    if($(this).scrollTop() === 0){
      let messageLoading = `<img src="images/chat/message-loading.gif" class="message-loading" />`;
      $(this).prepend(messageLoading);
      let targetId = $(this).data("chat");
      let skipMessage  = $(this).find("div.bubble").length;
      let chatInGroup = $(this).hasClass("chat-in-group") ? true : false;

      let thisDom = $(this);
        $.get(`message/read-more-message?skipMessage=${skipMessage}&targetId=${targetId}&chatInGroup=${chatInGroup}`, function(data){
          if(data.rightSideData.trim() == ""){
            alertify.notify("All messages showed", "error", 7);
           thisDom.find("img.message-loading").remove();
                return false;
          };
          // 1. handle rightSide
          $(`.right .chat[data-chat =${targetId}]`).prepend(data.rightSideData);
          // 2. prepend Scroll
          $(`.right .chat[data-chat =${targetId}]`).scrollTop(firstMessage.offset().top - currentOffset);

          // 3. convert emoji
          convertEmoji();
          // 4. handle image modal
          $(`#imagesModal_${targetId}`).find(".all-image").append(data.imagesModalSideData);
          // 5. call  gridPhotos(5);
          gridPhotos(5);
          // 6. handle attachment
          $(`attachmentsModal_${targetId}`).find("ul.list-attachments").append(data.attachmentModelSideData);
          // 7. remove message loading
          thisDom.find("img.message-loading").remove();
          zoomImageChat();


        });
    }
  })
};



$(document).ready(function(){
readMoreMessage();
})