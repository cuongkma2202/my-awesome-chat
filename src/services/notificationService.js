import Notification from "../models/notificationModel.js";
import userModel from "../models/userModel.js";

const LIMIT_NUMBER = 5;
/**
 * get notification when f5 page limit 10 item
 * @param {string} currentUserId
 */
const getNotificationsService = (currentUserId) => {
    //getByUserIdLimit
    return new Promise(async(resolve, reject) => {
        try {
            let notifications = await Notification.NotificationModel.getByUserIdLimit(currentUserId, LIMIT_NUMBER);

            let getNotifContent = notifications.map(async(notification) => {
                let sender = await userModel.getNormalUserDataById(notification.senderId);
                return Notification.NOTIFICATION_CONTENT.getContent(notification.type, notification.isRead, sender._id, sender.username, sender.avatar);
            });
            // console.log(getNotifContent);
            resolve(await Promise.all(getNotifContent));
        } catch (error) {
            reject(error);
        }
    })
};
/**
 * count all notif onread
 * @param {*} currentUserId
 * @param {*} limit
 * @returns
 */
const countNotifUnread = (currentUserId, limit = 10) => {

        return new Promise(async(resolve, reject) => {
            try {
                let notificationsUnread = await Notification.NotificationModel.countNotifUnread(currentUserId);
                resolve(notificationsUnread);
            } catch (error) {
                reject(error);
            }
        })
    }
    /**
     * Read more limit 10 item
     * @param {string} currentUserId
     * @param {number} skipNumberNotification
     * @returns
     */
const readMoreService = (currentUserId, skipNumberNotification) => {
    return new Promise(async(resolve, reject) => {
        try {
            let newNotifications = await Notification.NotificationModel.readMore(currentUserId, skipNumberNotification, LIMIT_NUMBER);

            let getNotifContent = newNotifications.map(async(notification) => {
                let sender = await userModel.getNormalUserDataById(notification.senderId);
                return Notification.NOTIFICATION_CONTENT.getContent(notification.type, notification.isRead, sender._id, sender.username, sender.avatar);
            });
            // console.log(getNotifContent);
            resolve(await Promise.all(getNotifContent));
        } catch (error) {
            reject(error)
        };
    });
};
/**
 *
 * @param {*} currentUserId
 * @param {array} targetUsers
 * @returns
 */
const markAllAsReadService = (currentUserId, targetUsers) => {
    // console.log(currentUserId);
    // console.log(targetUsers);
    return new Promise(async(resolve, reject) => {
        try {
            const check = await Notification.NotificationModel.markAllAsRead(currentUserId, targetUsers);
            // console.log(check);
            resolve(true);
        } catch (error) {
            console.log(`Error when mark notification as read: ${error}`);
            reject(false)
        }
    });
}
export {
    getNotificationsService,
    countNotifUnread,
    readMoreService,
    markAllAsReadService
}