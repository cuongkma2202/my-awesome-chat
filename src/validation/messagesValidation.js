import { check } from "express-validator";
import { transValidation } from "../../lang/vi.js";

const checkMessageLength = [
  check("messageVal", transValidation.message_text_emoji_err).isLength({ min: 1, max: 400 }),

]

export { checkMessageLength }