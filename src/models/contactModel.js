import mongoose from "mongoose";
const Schema = mongoose.Schema;

const ContactSchema = new Schema({
        userId: String,
        contactId: String,
        status: { type: Boolean, default: false },
    },

    { timestamps: true }
);
ContactSchema.statics = {
    createNew(item) {
        return this.create(item);
    },
    /*

    */
    findAllByUser(userId) {
        return this.find({
            $or: [
                { "userId": userId },
                { "contactId": userId }
            ]
        }).exec();
    },
    checkExists(userId, contactId) {
        return this.findOne({
            $or: [{
                    $and: [
                        { "userId": userId },
                        { "contactId": contactId },

                    ]
                },
                {
                    $and: [
                        { "userId": contactId },
                        { "contactId": userId },
                    ]
                }
            ]
        }).exec();
    },
    removeContact(userId, contactId) {
        return this.deleteOne({
            $or: [{
                    $and: [
                        { "userId": userId },
                        { "contactId": contactId },
                        { "status": true }

                    ]
                },
                {
                    $and: [
                        { "userId": contactId },
                        { "contactId": userId },
                        { "status": true }
                    ]
                }
            ]
        }).exec();
    },
    removeReqContactSent(userId, contactId) {
        return this.deleteOne({
            $and: [
                { "userId": userId },
                { "contactId": contactId },
                { "status": false }

            ]
        }).exec();
    },
    removeReqContactReceived(userId, contactId) {
        return this.deleteOne({
            $and: [
                { "contactId": userId },
                { "userId": contactId },
                { "status": false },

            ]
        }).exec();
    },
    /**
     *
     * @param {string} userId
     * @param {number} limit
     */
    getContacts(userId, limit) {
        return this.find({
            $and: [{
                    $or: [
                        { "userId": userId },
                        { "contactId": userId }
                    ]
                },
                { "status": true }
            ]
        }).sort({ "updatedAt": -1 }).limit(limit).exec();
    },
    getContactsSent(userId, limit) {
        return this.find({
            $and: [
                { "userId": userId },
                { "status": false }
            ]
        }).sort({ "createdAt": -1 }).limit(limit).exec();
    },
    getContactsReceived(userId, limit) {
        return this.find({
            $and: [
                { "contactId": userId },
                { "status": false }
            ]
        }).sort({ "createdAt": -1 }).limit(limit).exec();
    },
    countAllContacts(userId) {
        return this.count({
            $and: [{
                    $or: [
                        { "userId": userId },
                        { "contactId": userId }
                    ]
                },
                { "status": true }
            ]
        }).exec();
    },
    countAllContactsSent(userId) {
        return this.count({
            $and: [
                { "userId": userId },
                { "status": false }
            ]
        }).exec();
    },
    countAllContactsReceived(userId) {
        return this.count({
            $and: [
                { "contactId": userId },
                { "status": false }
            ]
        }).exec();
    },
    readMoreContacts(userId, skip, limit) {
        return this.find({
            $and: [{
                    $or: [
                        { "userId": userId },
                        { "contactId": userId }
                    ]
                },
                { "status": true }
            ]
        }).sort({ "updatedAt": -1 }).limit(limit).skip(skip).exec();
    },
    readMoreContactsSent(userId, skip, limit) {
        return this.find({
            $and: [
                { "userId": userId },
                { "status": false }
            ]
        }).sort({ "createdAt": -1 }).skip(skip).limit(limit).exec();
    },
    readMoreContactsReceived(userId, skip, limit) {
        return this.find({
            $and: [
                { "contactId": userId },
                { "status": false }
            ]
        }).sort({ "createdAt": -1 }).skip(skip).limit(limit).exec();
    },
    approveReqContactReceived(userId, contactId) {
        return this.updateOne({
            $and: [
                { "contactId": userId },
                { "userId": contactId },
                { "status": false }

            ]
        }, { "status": true }).exec();
    },
    updateWhenHasNewMessage(userId, contactId){
        return this.updateOne({
            $or: [{
                    $and: [
                        { "userId": userId },
                        { "contactId": contactId },
                    ]
                },
                {
                    $and: [
                        { "userId": contactId },
                        { "contactId": userId },
                    ]
                }
            ]
        },{"updatedAt": Date.now()}).exec();
    },
    getFriend(userId) {
        return this.find({
            $and: [{
                    $or: [
                        { "userId": userId },
                        { "contactId": userId }
                    ]
                },
                { "status": true }
            ]
        }).sort({ "updatedAt": -1 }).exec();
    },

}

const Contact = mongoose.model("contact", ContactSchema);

export default Contact;