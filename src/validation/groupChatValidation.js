import { check } from "express-validator";
import { transValidation } from "../../lang/vi.js";

const addNewGroupValidation = [
  check("arrayIds", transValidation.add_new_group_user_incorrect).
  custom((value)=>{
   if(!Array.isArray(value) ||value.length < 2 ){
      return false;
   };
   return true
  }),



  check("groupChatName", transValidation.add_new_group_name_incorrect).
  isLength({ min: 5, max: 30 })
  .matches(/^[\s0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/),


]


export {addNewGroupValidation}