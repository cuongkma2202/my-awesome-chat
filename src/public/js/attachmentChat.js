function attachmentChat(divId){
  $(`#attachment-chat-${divId}`).unbind("change").on("change", function(){
  const fileData = $(this).prop("files")[0];
  const limit = 5242880; // byte = 1mb



  if (fileData.size > limit) {
    alertify.notify("Upload file no larger than 5MB", "error", 7);
    $(this).val(null);
    return false;
  };
  let targetId = $(this).data("chat");
  let isChatGroup = false;

  let messageFormData = new FormData();

  messageFormData.append("my-attachment-chat", fileData);
  messageFormData.append("uid", targetId);

  if ($(this).hasClass("chat-in-group")) {
    messageFormData.append("isChatGroup", true);
    isChatGroup = true;
  };
  console.log(messageFormData);
  $.ajax({
    url: "/message/add-new-attachment",
    method: "post",
    cache: false,
    contentType: false,
    processData: false,
    data: messageFormData,
    success: function (data) {
      let dataToEmit = {
        message: data.message,
      };
      //1. handle message
      let divContent = $(`<div class="chat-content me" data-mess-id="${data.message._id}"></div>`);

      let messageOfMe = $(`
          <div class="bubble me bubble-attachment-file" data-mess-id="${data.message._id}"></div>
          `);
          let attachmentChat = `
          <a href="data: ${data.message.file.contentType}; base64, ${bufferToBase64(data.message.file.data.data)}" download="${data.message.file.fileName}">
          ${data.message.file.fileName}
          </a>
          `;
            if (isChatGroup) {
              let userInfo = `<div class="infor-user-chat me">
              <p>${data.message.sender.name}</p>
              <img src="/images/users/${data.message.sender.avatar}" class="avatar-small" title="${data.message.sender.name}"/>
             </div>`;
              divContent.html(`${userInfo}`).append( messageOfMe.html(`${attachmentChat}`));
              increaseNumberMessageGroup(divId);
              dataToEmit.groupId = targetId;
            } else {
              messageOfMe.html(attachmentChat);
              dataToEmit.contactId = targetId;
            }
         // 2. append message to screen
         if(isChatGroup){
          $(`.right .chat[data-chat=${divId}]`).append(divContent);
          nineScrollRight(divId);
        }else{
          $(`.right .chat[data-chat=${divId}]`).append(messageOfMe);
          nineScrollRight(divId)
        }

         // 3. remove input: nothing .
         // 4. change preview and timestamp in leftSide
         $(`.person[data-chat = ${divId}]`)
           .find("span.time").removeClass("message-realtime")
           .html(
             moment(data.message.createdAt)
               .locale("en")
               .startOf("seconds")
               .fromNow()
           );
         $(`.person[data-chat = ${divId}]`)
           .find("span.preview")
           .html("Attachment....");

            // 5. move conversation to top
            $(`.person[data-chat = ${divId}]`).click(
              "triggerEvent.moveConversationToTop",
              function () {
                let dataToMove = $(this).parent();
                $(this).closest("ul").prepend(dataToMove);
                $(this).off("triggerEvent.moveConversationToTop");
              }
            );
            $(`.person[data-chat = ${divId}]`).click();

            // 6. emit realtime
            socket.emit("chat-attachment", dataToEmit);
            // 7.emit remove typing realtime: nothing
            // 8.if this has typing remove remove gif: nothing
            // 9.add new attchment to model image
            let attachmentChatToAddModal = `
            <li>
            <a href="data: ${data.message.file.contentType}; base64, ${bufferToBase64(data.message.file.data.data)}" download="${data.message.file.fileName}">
            ${data.message.file.fileName}
            </a>
           </li>
            `
            $(`#attachmentsModal_${divId}`).find("ul.list-attachments").append(attachmentChatToAddModal);

        },
    error: function (error) {
      console.log(error);
      alertify.notify(error.responseText, "error", 7);
    }
  });

  });
};

$(document).ready(function(){
  socket.on("response-chat-attachment", function(response){
    let divId = "";
      let divContent = $(`<div class="chat-content you" data-mess-id="${response.message._id}"></div>`);
    messageOfYou = $(`
    <div class="bubble you bubble-attachment-file"  data-mess-id="${response.message._id}"></div>
    `);

    messageOfYou.text(response.message.text);

    let attachmentChat = `
    <a href="data: ${response.message.file.contentType}; base64, ${bufferToBase64(response.message.file.data.data)}" download="${response.message.file.fileName}">
    ${response.message.file.fileName}
    </a>
    `;
console.log(response);
    if (response.currentGroupId) {
      divId = response.currentGroupId;
      let userInfo = `<div class="infor-user-chat you">

              <img src="/images/users/${response.message.sender.avatar}" class="avatar-small" title="${response.message.sender.name}"/>
              <p>${response.message.sender.name}</p>
             </div>`;
              divContent.html(`${userInfo}`).append(messageOfYou.html(attachmentChat));
      if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
        increaseNumberMessageGroup(divId);
      }
    } else {
      divId = response.currentUserId;
      messageOfYou.html(attachmentChat);
    }

    //  2. append message to screen
     if(response.currentGroupId){
      if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
        $(`.right .chat[data-chat=${divId}]`).append(divContent);
        nineScrollRight(divId);
        $(`.person[data-chat = ${divId}]`)
        .find("span.time")
        .addClass("message-realtime")
      }
    }else{
      if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
        $(`.right .chat[data-chat=${divId}]`).append(messageOfYou);
        nineScrollRight(divId);
        $(`.person[data-chat = ${divId}]`)
        .find("span.time")
        .addClass("message-realtime")
      }
    }

    // 3. remove input :nothing
    // 4. change preview and timestamp in leftSide
    $(`.person[data-chat = ${divId}]`)
      .find("span.time")
      .html(
        moment(response.message.createdAt)
          .locale("en")
          .startOf("seconds")
          .fromNow()
      );
    $(`.person[data-chat = ${divId}]`)
      .find("span.preview")
      .html("Attachment....");


      // 5. move conversation to top
    $(`.person[data-chat = ${divId}]`).click(
      "triggerEvent.moveConversationToTop",
      function () {
        let dataToMove = $(this).parent();
        $(this).closest("ul").prepend(dataToMove);
        $(this).off("triggerEvent.moveConversationToTop");
      }
    );
    $(`.person[data-chat = ${divId}]`).trigger("triggerEvent.moveConversationToTop");




        // 9.add new image to model image

        if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
          let attachmentChatToAddModal = `
          <li>
          <a href="data: ${response.message.file.contentType}; base64, ${bufferToBase64(response.message.file.data.data)}" download="${response.message.file.fileName}">
          ${response.message.file.fileName}
          </a>
         </li>
          `;
          $(`#attachmentsModal_${divId}`).find("ul.list-attachments").append(attachmentChatToAddModal);
        }
  });
  });