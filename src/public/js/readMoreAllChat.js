$(document).ready(function() {
  $("#link-read-more-all-chat").bind("click", function() {
      let skipPersonalNumber = $("#all-chat").find("li:not(.group-chat)").length;
      let skipGroupNumber = $("#all-chat").find("li.group-chat").length;



      $("#link-read-more-all-chat").css("display", "none");
      $(".read-more-all-chat-loader").css("display", "inline-block");
      setTimeout(() => {
          $.get(`/message/read-more-all-chat?skipPersonalNumber=${skipPersonalNumber}&skipGroupNumber=${skipGroupNumber}`, function(data) {
            console.log(data);
              if(data.leftSideData.trim() == ""){
                alertify.notify("All conversations showed", "error", 7);
                    $("#link-read-more-all-chat").css("display", "inline-block");
                    $(".read-more-all-chat-loader").css("display", "none");
                    return false;
              };
              // 1. handle leftSide
              $("#all-chat").find("ul").append(data.leftSideData);
              // 2. scroll left
              resizeNineScrollLeft();
              nineScrollLeft();
              // 3. handle rightSide
              $("#screen-chat").append(data.rightSideData);
              // 4. chan screenChat - mainConfig.js
              changeScreenChat();
              // 5.call convert Emoji
              convertEmoji();
              // 6. handle images modal
              $("body").append(data.imagesModalSideData);
              // 7.  call gridPhotos(5);
              gridPhotos(5);
              // 8. handle attachment
              $("body").append(data.attachmentModelSideData);
                //handle member in group chat


              // 9. check Online
              socket.emit("check-status");
              // =======================//
              //10. remove loading
              $("#link-read-more-all-chat").css("display", "inline-block");
              $(".read-more-all-chat-loader").css("display", "none");
              //11. call Read More Message
              readMoreMessage();

              zoomImageChat();
              $("body").append(data.membersModalData);
              userTalk();
          });
      }, 500);

  })
})