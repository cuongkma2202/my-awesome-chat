import passport from "passport";
import passportLocal from "passport-local";
import User from "../../models/userModel.js";
import { transError, transSuccess } from "../../../lang/vi.js";
import ChatGroupModel from "../../models/chatGroupModel.js"
import bcrypt from "bcrypt";

const LocalStrategy = passportLocal.Strategy;
/**
 * Valid user account type
 */
let initPassportLocal = () => {
    passport.use(
        new LocalStrategy({
                usernameField: "email",
                passwordField: "password",
                passReqToCallback: true,
            },
            async(req, email, password, done) => {
                try {
                    const user = await User.findByEmail(email);
                    if (!user) {
                        return done(
                            null,
                            false,
                            req.flash("errors", transError.login_failed)
                        );
                    }
                    if (!user.local.isActive) {
                        return done(
                            null,
                            false,
                            req.flash("errors", transError.not_active)
                        );
                    }
                    const checkPassword = await user.comparePassword(password);
                    if (!checkPassword) {
                        return done(
                            null,
                            false,
                            req.flash("errors", transError.login_failed)
                        );
                    }
                    return done(
                        null,
                        user,
                        req.flash("success", transSuccess.loginSuccess(user.username))
                    );
                } catch (error) {
                    // console.log(error);
                    return done(
                        null,
                        false,
                        req.flash("errors", transError.server_error)
                    );
                }
            }
        )
    );
    // Save userID to Session
    passport.serializeUser((user, done) => {
        done(null, user._id);
    });
    passport.deserializeUser(async (id, done) => {
        try {
            let user = await User.findUserByIdForSessionToUse(id);
            let getChatGroupIds = await ChatGroupModel.getChatGroupIdsByUser(user._id);

            user = user.toObject();
            user.chatGroupIds = getChatGroupIds;

            return done(null, user);
          } catch (error) {
            return done(error, null);
          }
    });
};
export { initPassportLocal };