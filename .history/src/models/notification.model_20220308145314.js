import { Schema, model } from "mongoose";

const NotificationSchema = new Schema(
  {
    sender: { id: String, username: String, avatar: String },
    receiver: { id: String, username: String, avatar: String },
    type: String,
    content: String,
    isRead: { type: Boolean, default: fault },
  },
  { timestamps: true }
);
module.exports = model("message", NotificationSchema);
