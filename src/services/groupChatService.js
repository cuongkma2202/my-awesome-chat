import _ from "lodash";
import ChatGroupModel from "../models/chatGroupModel.js"
let addNewGroupService = (currentUserId, arrayMemberIds, groupChatName) => {
return new Promise(async (resolve, reject)=>{
try {
// add currentUserId to arrayMember;
  arrayMemberIds.unshift({userId: `${currentUserId}`});

  arrayMemberIds = _.uniqBy(arrayMemberIds, "userId");

let newGroupItem = {
    name: groupChatName,
    userAmount: arrayMemberIds.length ,
    userId: `${currentUserId}`,
    members: arrayMemberIds,
};
let newGroup = await ChatGroupModel.createNew(newGroupItem);
resolve(newGroup);

} catch (error) {
  console.log(error);
  reject(error);
}
})
};

export {addNewGroupService}