import { pushSocketIdToArray, emitNotifyToArray, removeSocketIdFromArray } from "../../helpers/socketHelper.js";
/**
 *
 * @param io from socket.io
 */
const userOnlineOfflineSocket = (io) => {
  let clients = {};
  io.on("connection", (socket) => {
    clients = pushSocketIdToArray(clients, socket.request.user._id, socket.id);
    socket.request.user.chatGroupIds.forEach(group=>{

      clients = pushSocketIdToArray(clients, group._id, socket.id);
    });
      // has new group chat
      socket.on("new-group-created", (data) => {
        clients = pushSocketIdToArray(clients, data.groupChat._id, socket.id);
      });
      socket.on("member-received-group-chat", (data)=>{
        clients = pushSocketIdToArray(clients, data.groupChatId, socket.id);
      });
    socket.on("check-status", function(){
      let listUserOnline = Object.keys(clients);
      //  step1: => emit to user after login or refresh web page

      socket.emit("server-send-list-user-online", listUserOnline);

      // step2: => emit to all another when has new another online
      socket.broadcast.emit("server-send-when-new-user-online", socket.request.user._id);
    });
    socket.on("disconnect", () => {
      clients = removeSocketIdFromArray(clients, socket.request.user._id, socket);
      socket.request.user.chatGroupIds.forEach(group=>{
        clients = removeSocketIdFromArray(clients, group._id, socket);
      });
      // step3: => when user disconnect => emit to all => when user offline
      socket.broadcast.emit("server-send-when-new-user-offline", socket.request.user._id);

    });

  });
};
export default userOnlineOfflineSocket;