const app = {
  avatar_dir: "src/public/images/users",
  avatar_type: ["image/png", "image/jpg", "image/jpeg"],
  avatar_limit_size: 1048576,
  image_message_dir: "src/public/images/chat/message",
  image_message_type: ["image/png", "image/jpg", "image/jpeg"],
  image_message_limit_size: 1048576,
  attachment_message_dir: "src/public/images/chat/message",
  attachment_message_limit_size: 5242880,
  general_avatar_group_chat: "group-avatar-trungquandev.png"
}
export default app;