import { validationResult } from "express-validator";
import ejs from "ejs";
import { lastItemOfArray, convertTimestamp, bufferToBase64 } from "../helpers/clientHelper.js";
import _ from "lodash";
import {
  addNewTextEmojiService,
  addNewImageMessageService,
  addNewAttachmentMessageService,
  readMoreAllChatService,
  readMoreMessageService,
  searchConversationService,
  readMorePersonalChatService,
  readMoreGroupChatService
} from "../services/messageService.js";
import multer from "multer";
import app from "../config/app.js";
import { transError, transSuccess } from "../../lang/vi.js";
import { promisify } from "util";

// convert EJS function RENDER_FILE to PROMISE - ASYNC AWAIT
const renderFile = promisify(ejs.renderFile).bind(ejs);
const addNewTextEmoji = async (req, res) => {
  let errorArr = [];
  const validationError = validationResult(req);
  if (!validationError.isEmpty()) {
    let errors = Object.values(validationError.mapped());
    // console.log(errors);
    errorArr = errors.map((item) => {
      return item.msg;
    });
    return res.status(500).send(errorArr);
  }
  try {
    let sender = {
      id: req.user._id,
      name: req.user.username,
      avatar: req.user.avatar,
    };
    let receiverId = req.body.uid;
    let messageVal = req.body.messageVal;
    let isChatGroup = req.body.isChatGroup;
    // console.log({messageVal,receiverId,isChatGroup});
    let newMessage = await addNewTextEmojiService(
      sender,
      receiverId,
      messageVal,
      isChatGroup
    );
    res.status(200).send({ message: newMessage });
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};
import fsExtra from "fs-extra";

let storageImageMessage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, app.image_message_dir);
  },
  filename: (req, file, callback) => {
    let math = app.image_message_type;
    if (math.indexOf(file.mimetype) === -1) {
      return callback(transError.image_message_type_error, null);
    }
    let imageMessageName = `${file.originalname}`;
    callback(null, imageMessageName);
  },
});
const imageMessageUploadFile = multer({
  storage: storageImageMessage,
  limits: {
    fileSize: app.image_message_limit_size,
  },
}).single("my-image-chat");

const addNewImageMessage = (req, res) => {
  imageMessageUploadFile(req, res, async (error) => {
    if (error) {
      if (error.message) {
        console.log(error);
        return res.status(500).send(transError.image_message_size_error);
      }
      return res.status(500).send(error);
    }
    try {
      let sender = {
        id: req.user._id,
        name: req.user.username,
        avatar: req.user.avatar,
      };
      let receiverId = req.body.uid;
      let messageVal = req.file;
      let isChatGroup = req.body.isChatGroup;
      // console.log({messageVal,receiverId,isChatGroup});
      let newMessage = await addNewImageMessageService(
        sender,
        receiverId,
        messageVal,
        isChatGroup
      );
      // remove img, because image storage in DB

      await fsExtra.remove(
        `${app.image_message_dir}/${newMessage.file.fileName}`
      );
      res.status(200).send({ message: newMessage });
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  });
};

let storageAttachmentMessage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, app.attachment_message_dir);
  },
  filename: (req, file, callback) => {
    let attachmentMessageName = `${file.originalname}`;
    callback(null, attachmentMessageName);
  },
});
const attachmentMessageUploadFile = multer({
  storage: storageAttachmentMessage,
  limits: {
    fileSize: app.attachment_message_limit_size,
  },
}).single("my-attachment-chat");
const addNewAttachmentMessage = (req, res) => {
  attachmentMessageUploadFile(req, res, async (error) => {
    if (error) {
      if (error.message) {
        return res.status(500).send(transError.attachment_message_size_error);
      }
      return res.status(500).send(error);
    }
    try {
      let sender = {
        id: req.user._id,
        name: req.user.username,
        avatar: req.user.avatar,
      };
      let receiverId = req.body.uid;
      let messageVal = req.file;
      let isChatGroup = req.body.isChatGroup;
      console.log(receiverId);
      console.log(messageVal);
      console.log(isChatGroup);
      let newMessage = await addNewAttachmentMessageService(
        sender,
        receiverId,
        messageVal,
        isChatGroup
      );
      // remove img, because image storage in DB

      await fsExtra.remove(
        `${app.image_message_dir}/${newMessage.file.fileName}`
      );
      res.status(200).send({ message: newMessage });
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  });
};
const readMoreAllChat = async (req, res) => {
  try {
    // get skip number from query
    let skipPersonalNumber = +req.query.skipPersonalNumber;
    let skipGroupNumber = +req.query.skipGroupNumber;
    // get moreItem
    let newAllConversations = await readMoreAllChatService(
      req.user._id,
      skipPersonalNumber,
      skipGroupNumber
    );
    let dataToRender = {
      newAllConversations: newAllConversations,
      lastItemOfArray: lastItemOfArray,
      convertTimestamp: convertTimestamp,
      bufferToBase64: bufferToBase64,
      user: req.user


    }
    let leftSideData = await renderFile("src/views/main/readMoreConversations/_leftSide.ejs", dataToRender);
    let rightSideData = await renderFile("src/views/main/readMoreConversations/_rightSide.ejs", dataToRender);
    let attachmentModelSideData = await renderFile("src/views/main/readMoreConversations/_attachmentModal.ejs", dataToRender);
    let imagesModalSideData = await renderFile("src/views/main/readMoreConversations/_imageModal.ejs", dataToRender);
    let membersModalData = await renderFile("src/views/main/readMoreConversations/_membersModal.ejs", dataToRender);
    return res
      .status(200)
      .send({
        leftSideData: leftSideData,
        rightSideData: rightSideData,
        attachmentModelSideData: attachmentModelSideData,
        imagesModalSideData: imagesModalSideData,
        membersModalData:membersModalData
      });
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};
const readMoreMessage = async (req, res) => {
  // skipMessage targetId chatInGroup
  try {

    // get skip number from query
    let skipMessage = +req.query.skipMessage;
    let targetId = req.query.targetId;
    let chatInGroup = (req.query.chatInGroup) === "true" ? true : false;
    // get moreItem
    let newMessage = await readMoreMessageService(
      req.user._id,
      skipMessage,
      targetId,
      chatInGroup
    );
    let dataToRender = {
      newMessage: newMessage,
      bufferToBase64: bufferToBase64,
      user: req.user
    }
    let rightSideData = await renderFile("src/views/main/readMoreMessages/_rightSide.ejs", dataToRender);
    let attachmentModelSideData = await renderFile("src/views/main/readMoreMessages/_attachmentModal.ejs", dataToRender);
    let imagesModalSideData = await renderFile("src/views/main/readMoreMessages/_imageModal.ejs", dataToRender);
    return res
      .status(200)
      .send({
        rightSideData: rightSideData,
        attachmentModelSideData: attachmentModelSideData,
        imagesModalSideData: imagesModalSideData,
      });

  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};
let searchConversation = async (req, res) => {
  let errorsArr = [];
  let validationErrors = validationResult(req);

  if (!validationErrors.isEmpty()) {
    let errors = Object.values(validationErrors.mapped());
    errors.forEach((item) => {
      errorsArr.push(item.msg);
    });
    // Logging when validation errors, this can show to normal user
    // console.log(errorsArr);
    return res.status(500).send(errorsArr);
  }

  try {
    let currentUserId = req.user._id;
    let keyword = req.params.keyword;
    let allConversations = await searchConversationService(currentUserId, keyword);
    allConversations =  _.flatten(allConversations);
    return res.render("main/extras/_searchConversation", {allConversations});
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};
const readMorePersonalChat = async (req, res) => {
  try {
    // get skip number from query
    let skipPersonalNumber = +req.query.skipPersonalNumber;
    // get moreItem
    let newPersonalConversation = await readMorePersonalChatService(
      req.user._id,
      skipPersonalNumber,
    );
    let dataToRender = {
      newPersonalConversation: newPersonalConversation,
      lastItemOfArray: lastItemOfArray,
      convertTimestamp: convertTimestamp,
      bufferToBase64: bufferToBase64,
      user: req.user

    }
    let leftSideData = await renderFile("src/views/main/extras/readMorePersonal/_leftSide.ejs", dataToRender);
    let rightSideData = await renderFile("src/views/main/extras/readMorePersonal/_rightSide.ejs", dataToRender);
    let attachmentModelSideData = await renderFile("src/views/main/extras/readMorePersonal/_attachmentModal.ejs", dataToRender);
    let imagesModalSideData = await renderFile("src/views/main/extras/readMorePersonal/_imageModal.ejs", dataToRender);
    return res
      .status(200)
      .send({
        leftSideData: leftSideData,
        rightSideData: rightSideData,
        attachmentModelSideData: attachmentModelSideData,
        imagesModalSideData: imagesModalSideData,
      });
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};
const readMoreGroupChat = async (req, res) => {
  try {
    // get skip number from query
    let skipGroupNumber = +req.query.skipGroupNumber;
    // get moreItem
    let newGroupConversation = await readMoreGroupChatService(
      req.user._id,
      skipGroupNumber
    );
    let dataToRender = {
      newGroupConversation: newGroupConversation,
      lastItemOfArray: lastItemOfArray,
      convertTimestamp: convertTimestamp,
      bufferToBase64: bufferToBase64,
      user: req.user


    }
    let leftSideData = await renderFile("src/views/main/extras/readMoreGroup/_leftSide.ejs", dataToRender);
    let rightSideData = await renderFile("src/views/main/extras/readMoreGroup/_rightSide.ejs", dataToRender);
    let attachmentModelSideData = await renderFile("src/views/main/extras/readMoreGroup/_attachmentModal.ejs", dataToRender);
    let imagesModalSideData = await renderFile("src/views/main/extras/readMoreGroup/_imageModal.ejs", dataToRender);
    let membersModalData = await renderFile("src/views/main/extras/readMoreGroup/_membersModal.ejs", dataToRender);


    return res
      .status(200)
      .send({
        leftSideData: leftSideData,
        rightSideData: rightSideData,
        attachmentModelSideData: attachmentModelSideData,
        imagesModalSideData: imagesModalSideData,
        membersModalData:membersModalData
      });
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};
export {
  addNewTextEmoji,
  addNewImageMessage,
  addNewAttachmentMessage,
  readMoreAllChat,
  readMoreMessage,
  searchConversation,
  readMorePersonalChat,
  readMoreGroupChat
};
