import { transError } from "../../lang/vi.js";
import UserModel from "../models/userModel.js";
import bcrypt from 'bcrypt';
import { env } from "../config/environment.js"

const updateUserService = (id, item) => {
  return UserModel.updateUser(id, item);
}
const updatePasswordService = (id, dataUpdate) => {
  return new Promise(async (resolve, reject) => {
    let currentUser = await UserModel.findUserByIdToUpdatePassword(id);
    if (!currentUser) {
      return reject(transError.account_undefined);
    }
    let checkCurrentPassword = await currentUser.comparePassword(dataUpdate.currentPassword);
    if (!checkCurrentPassword) {
      return reject(transError.user_current_password_error);
    }
    const hashedPassword = await bcrypt.hash(
      dataUpdate.newPassword,
      parseInt(env.SALT_ROUND)
    );
    await UserModel.updatePassword(id, hashedPassword);
    resolve(true)
  })
}
export { updateUserService, updatePasswordService }