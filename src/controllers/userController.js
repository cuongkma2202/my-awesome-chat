import multer from "multer";
import app from "../config/app.js";
import { transError, transSuccess } from "../../lang/vi.js";
import { v4 as uuidv4 } from "uuid";
import { updatePasswordService, updateUserService } from "../services/userService.js";
import { validationResult } from "express-validator";
import fsExtra from "fs-extra";

let storageAvatar = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, app.avatar_dir);
  },
  filename: (req, file, callback) => {
    let math = app.avatar_type;
    if (math.indexOf(file.mimetype) === -1) {
      return callback(transError.avatar_type_error, null);
    }
    let avatarName = `${Date.now()}-${uuidv4()}-${file.originalname}`;
    callback(null, avatarName);

  }
});
const avatarUploadFile = multer({
  storage: storageAvatar,
  limits: {
    fileSize: app.avatar_limit_size,
  }
}).single("avatar")



const updateAvatar = (req, res) => {
  avatarUploadFile(req, res, async (error) => {
    if (error) {
      if (error.message) {
        return res.status(500).send(transError.avatar_size_error);
      }
      return res.status(500).send(error);
    }
    try {
      let updateUserItem = {
        avatar: req.file.filename
      }
      // Update user
      let userUpdate = await updateUserService(req.user._id, updateUserItem);
      // Remove old avatar
      // await fsExtra.remove(`${app.avatar_dir}/${userUpdate.avatar}`);
      let result = {
        message: transSuccess.userinfor_update_success,
        imageSrc: `/images/users/${req.file.filename}`,
      };
      return res.status(200).send(result);
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);

    }
  })
}
const updateUserInformation = async (req, res) => {
  let errorArr = [];
  const validationError = validationResult(req);
  if (!validationError.isEmpty()) {
    let errors = Object.values(validationError.mapped());
    // console.log(errors);
    errorArr = errors.map((item) => {
      return item.msg;
    });
    return res.status(500).send(errorArr);
  }

  try {
    let updateUserItem = req.body;

    let userUpdate = await updateUserService(req.user._id, updateUserItem);
    let result = {
      message: transSuccess.userinfor_update_success,
    };
    return res.status(200).send(result);
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
}
const userUpdatePassword = async (req, res) => {
  let errorArr = [];
  const validationError = validationResult(req);
  if (!validationError.isEmpty()) {
    let errors = Object.values(validationError.mapped());
    // console.log(errors);
    errorArr = errors.map((item) => {
      return item.msg;
    });
    return res.status(500).send(errorArr);
  }
  try {
    let updateUserItem = req.body;
    await updatePasswordService(req.user._id, updateUserItem);
    let result = {
      message: transSuccess.user_password_update_success,
    };
    return res.status(200).send(result);
  } catch (error) {
    return res.status(500).send(error)
  }
}
export { updateAvatar, updateUserInformation, userUpdatePassword }