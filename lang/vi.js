const transValidation = {
    email_incorrect: "You have entered an invalid email. Please try again",
    gender_incorrect: "Your gender undefine",
    password_incorrect: "Minium of different classes of characters in password is 8. Classes of character: digits, lowercase characters and special characters.",
    password_confirmation_incorrect: "Password confirmation does not match ",
    update_username: "This field must be maximum length of 15 and no special characters",
    update_gender: "Only accepted Male and Female. LGBT deny",
    update_address: "This field must be maximum length of 30 and no special characters",
    update_phone: "Invalid phone number",
    user_current_password_error: "Current password incorrect",
    user_confirm_password_error: "Confirm password dose not match",
    keyword_find_error: "Invalid Keyword",
    message_text_emoji_err: "Error message. Min 1 max 400",
    add_new_group_user_incorrect: "Please add more member to add group",
    add_new_group_name_incorrect: "Invalid name !!",
    keyword_find_user: "Invalid keyword"
};
const transError = {
    email_in_use: "Email already taken. Please try again",
    not_active: "Please active your email first",
    token_null: "Token undefined!",
    login_failed: "Incorrect email address or password",
    server_error: "Oops. Something went wrong. Try to refresh this page or feel free to contact us id the problem persists",
    avatar_type_error: "File does not support. JPG, PNG only.",
    avatar_size_error: "Avatar size to large. Upload limit 1MB",
    account_undefined: "Account Undefined",
    user_current_password_error: "Current password incorrect",
    conversation_not_found: "Conversation not found",
    image_message_type_error: "File does not support. JPG, PNG only.",
    image_message_size_error: "Image size to large. Upload limit 1MB",
    attachment_message_size_error: "Attachment size to large. Upload limit 5MB",
};
const transSuccess = {
    userCreated(email) {
        return `Congratulation <strong>${email}</strong>, your account has been successfully created. Please check your email to active. `;
    },
    account_activated: "Your account has been activated. You may login to Chat",
    loginSuccess: (username) => {
        return `Hello ${username}. Welcome to Chat`;
    },
    logout_success: "Logout Successfully",
    userinfor_update_success: "Profile updated!",
    user_password_update_success: "Change password success",

};
const transMail = {
    subject: "Awesome Chat: Active Email",
    template: (linkVerify, name) => {
        return ` <h2> Awesome Chat</h2>
             <p>Hi ${name}</p>
             <p>We're happy you signed up for Awesome Chat.</p>
             <p>To start exploring the Chat please confirm your email Address</p>
             <h3><a href="${linkVerify}" target="blank">Click Here</a></h3>
             <h4> Welcome to Awesome Chat</h4>
             <h4>Cuong Nguyen</h4>
       `;
    },
    send_fail: "Send mail fail. Please try again",
};
export { transValidation, transError, transSuccess, transMail };