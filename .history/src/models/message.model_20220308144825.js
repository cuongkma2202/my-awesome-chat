import { Schema, model } from "mongoose";

const MessageSchema = new Schema(
  {
    sender: { id: String, username: String, avatar: String },
    receiver: { id: String, username: String, avatar: String },
    text: String,
    file: { data: Buffer, contentType: String, fileName: String },
  },

  { timestamps: true }
);
module.exports = model("message", MessageSchema);
