import mongoose from "mongoose";
const Schema = mongoose.Schema;

const MessageSchema = new Schema(
  {
    senderId: String,
    receiverId: String,
    conversationType: String,
    messageType: String,
    sender: { id: String, name: String, avatar: String },
    receiver: { id: String, name: String, avatar: String },
    text: String,
    file: { data: Buffer, contentType: String, fileName: String },
  },
  { timestamps: true }
);
const MESSAGE_CONVERSATION_TYPE = {
  PERSONAL: "personal",
  GROUP: "group",
};
const MESSAGE_TYPE = {
  TEXT: "text",
  IMAGE: "image",
  FILE: "file",
};
MessageSchema.statics = {
  /**
   * createNewMessage
   * @param {object} item
   * @returns
   */
  createNew(item) {
    return this.create(item);
},
  /**
   *
   * @param {string} senderId //currentUserId
   * @param {string} receivedId
   * @param {number} limit
   */
  getMessagesInPersonal(senderId, receiverId, limit) {
    return this.find({
      $or: [
        { $and: [{ senderId: senderId }, { receiverId: receiverId }] },
        { $and: [{ senderId: receiverId }, { receiverId: senderId }] },
      ],
    })
      .sort({ createdAt: -1 })
      .limit(limit)
      .exec();
  },
  /**
   *
   * @param {string} receivedId id group chat
   * @param {number} limit
   */
  getMessagesInGroup(receiverId, limit) {
    return this.find({ receiverId: receiverId })
      .sort({ createdAt: -1 })
      .limit(limit)
      .exec();
  },
  readMoreMessagesInPersonal(senderId, receiverId,skip, limit) {
    return this.find({
      $or: [
        { $and: [{ senderId: senderId }, { receiverId: receiverId }] },
        { $and: [{ senderId: receiverId }, { receiverId: senderId }] },
      ],
    })
      .sort({ createdAt: -1 })
      .skip(skip)
      .limit(limit)
      .exec();
  },
  readMoreMessagesInGroup(receiverId, skip,limit) {
    return this.find({ receiverId: receiverId })
      .sort({ createdAt: -1 })
      .skip(skip)
      .limit(limit)
      .exec();
  },
};
const MessageModel = mongoose.model("message", MessageSchema);
export default { MessageModel, MESSAGE_CONVERSATION_TYPE, MESSAGE_TYPE };
