import { validationResult } from "express-validator";
import { registerService, verifyAccount } from "../services/authService.js";
import { transSuccess } from "../../lang/vi.js";
const getLoginRegister = (req, res) => {
    return res.render("auth/master", {
        errors: req.flash("errors"),
        success: req.flash("success"),
    });
};
const postRegister = async(req, res) => {
    let errorArr = [];
    let successArr = [];
    const { email, gender, password } = req.body;
    const protocol = req.protocol;
    const host = req.get("host");

    const validationError = validationResult(req);
    if (!validationError.isEmpty()) {
        let errors = Object.values(validationError.mapped());
        // console.log(errors);
        errorArr = errors.map((item) => {
            return item.msg;
        });
        req.flash("errors", errorArr);
        return res.redirect("/login-register");
    }
    try {
        const createUserSuccess = await registerService(
            email,
            gender,
            password,
            protocol,
            host
        );
        successArr.push(createUserSuccess);
        req.flash("success", successArr);
        return res.redirect("/login-register");
    } catch (error) {
        errorArr.push(error);
        req.flash("errors", errorArr);
        return res.redirect("/login");
    }
};
const verifyAcc = async(req, res) => {
    let errorArr = [];
    let successArr = [];
    try {
        let verifySuccess = await verifyAccount(req.params.token);
        // console.log(req.params.token);
        successArr.push(verifySuccess);
        req.flash("success", successArr);
        return res.redirect("/login-register");
    } catch (error) {
        errorArr.push(error);
        req.flash("errors", errorArr);
        return res.redirect("/login-register");
    }
};
const getLogout = (req, res) => {
    req.logout(); // remove session passport user
    req.flash("success", transSuccess.logout_success);
    return res.redirect("/login-register");
};

const checkLoggedIn = (req, res, next) => {
    if (!req.isAuthenticated()) {
        return res.redirect("/login-register");
    }
    next();
};
const checkLoggedOut = (req, res, next) => {
    if (req.isAuthenticated()) {
        return res.redirect("/");
    }
    next();
};
export {
    getLoginRegister,
    postRegister,
    verifyAcc,
    getLogout,
    checkLoggedIn,
    checkLoggedOut,
};