import ContactModel from "../models/contactModel.js";
import Notification from "../models/notificationModel.js";
import UserModel from "../models/userModel.js";
import _ from "lodash";
const LIMIT_NUMBER = 10;
const findUserContactService = (currentUserId, keyword) => {
    return new Promise(async(resolve, reject) => {
        let deprecatedUserIds = [currentUserId];
        let contactsByUser = await ContactModel.findAllByUser(currentUserId);
        contactsByUser.forEach((contact) => {
            deprecatedUserIds.push(contact.userId);
            deprecatedUserIds.push(contact.contactId);
        });
        deprecatedUserIds = _.uniqBy(deprecatedUserIds);
        let users = await UserModel.findAllForAddContact(deprecatedUserIds, keyword);
        resolve(users);

    })
};
const addNewContactService = (currentUserId, contactId) => {
    return new Promise(async(resolve, reject) => {
        let contactExists = await ContactModel.checkExists(currentUserId, contactId);
        if (contactExists) {
            return reject(false);
        }
        // create contact
        let newContactItem = {
            userId: currentUserId,
            contactId: contactId,
        }
        let newContact = await ContactModel.createNew(newContactItem);
        // create notif
        let notificationItem = {
            senderId: currentUserId,
            receiverId: contactId,
            type: Notification.NOTIFICATION_TYPES.ADD_CONTACT,
        }
        await Notification.NotificationModel.createNew(notificationItem);
        resolve(newContact);

    })
};
const removeContactContactService = (currentUserId, contactId) => {
    return new Promise(async(resolve, reject) => {
        let removeContact = await ContactModel.removeContact(currentUserId, contactId);
        // console.log("result", removeContact);
        if (removeContact.deletedCount === 0) {
            reject(false);
        }
        resolve(true);

    })
}
const removeReqContactSentService = (currentUserId, contactId) => {
    return new Promise(async(resolve, reject) => {
        let removeReq = await ContactModel.removeReqContactSent(currentUserId, contactId);
        // console.log("result", removeReq);
        if (removeReq.deletedCount === 0) {
            reject(false);
        }
        // remove notification
        await Notification.NotificationModel.removeReqContactSentNotification(currentUserId, contactId, Notification.NOTIFICATION_TYPES.ADD_CONTACT);

        resolve(true);

    })
};
const removeReqContactReceivedService = (currentUserId, contactId) => {
    return new Promise(async(resolve, reject) => {
        let removeReq = await ContactModel.removeReqContactReceived(currentUserId, contactId);
        // console.log("result", removeReq);
        if (removeReq.deletedCount === 0) {
            reject(false);
        }
        // remove notification - feature coming soon
        // await Notification.NotificationModel.removeReqContactReceivedNotification(currentUserId, contactId, Notification.NOTIFICATION_TYPES.ADD_CONTACT);

        resolve(true);

    })
};
const approveReqContactReceivedService = (currentUserId, contactId) => {
    return new Promise(async(resolve, reject) => {
        let approveReq = await ContactModel.approveReqContactReceived(currentUserId, contactId);
        // console.log("result", approveReq);

        if (approveReq.modifiedCount === 0) {
            reject(false);
        }
        // create notification
        let notificationItem = {
            senderId: currentUserId,
            receiverId: contactId,
            type: Notification.NOTIFICATION_TYPES.APPROVE_CONTACT,
        }
        await Notification.NotificationModel.createNew(notificationItem);

        resolve(true);

    })
};
const getContactServices = (currentUserId) => {
    return new Promise(async(resolve, reject) => {
        try {
            // console.log(currentUserId);
            let contacts = await ContactModel.getContacts(currentUserId, LIMIT_NUMBER);
            // console.log(contacts);
            let users = contacts.map(async(contact) => {
                if (contact.contactId == currentUserId) {
                    return await UserModel.getNormalUserDataById(contact.userId);
                } else {
                    return await UserModel.getNormalUserDataById(contact.contactId);
                }
            });
            let usersArr = await Promise.all(users);

            usersArr = usersArr.slice().reverse()
                .map(function(item) {
                    return item
                });

            resolve(usersArr);
        } catch (error) {
            reject(error);
        }

    })
};
const getContactSentServices = (currentUserId) => {
    return new Promise(async(resolve, reject) => {
        try {
            let contacts = await ContactModel.getContactsSent(currentUserId, LIMIT_NUMBER);
            // console.log(contacts);
            let users = contacts.map(async(contact) => {
                return await UserModel.getNormalUserDataById(contact.contactId);
            });
            let usersArr = await Promise.all(users);

            usersArr = usersArr.slice().reverse()
                .map(function(item) {
                    return item
                });

            resolve(usersArr);

        } catch (error) {
            reject(error);
        }

    })
};
const getContactReceivedServices = (currentUserId) => {
    return new Promise(async(resolve, reject) => {
        try {
            let contacts = await ContactModel.getContactsReceived(currentUserId, LIMIT_NUMBER);
            // console.log(contacts);
            let users = contacts.map(async(contact) => {
                return await UserModel.getNormalUserDataById(contact.userId);
            });
            let usersArr = await Promise.all(users);

            usersArr = usersArr.slice().reverse()
                .map(function(item) {
                    return item
                });

            resolve(usersArr);
        } catch (error) {
            reject(error);
        }

    })
};
const countAllContactsService = (currentUserId) => {

    return new Promise(async(resolve, reject) => {
        try {
            let countAllContact = await ContactModel.countAllContacts(currentUserId);
            resolve(countAllContact);
        } catch (error) {
            reject(error);
        }
    })
};
const countAllContactsSentService = (currentUserId) => {

    return new Promise(async(resolve, reject) => {
        try {
            let countAllContact = await ContactModel.countAllContactsSent(currentUserId);
            resolve(countAllContact);
        } catch (error) {
            reject(error);
        }
    })
};
const countAllContactsReceivedService = (currentUserId) => {

    return new Promise(async(resolve, reject) => {
        try {
            let countAllContact = await ContactModel.countAllContactsReceived(currentUserId);
            resolve(countAllContact);
        } catch (error) {
            reject(error);
        }
    })
};
const readMoreContactService = (currentUserId, skipNumberContacts) => {
    return new Promise(async(resolve, reject) => {
        try {
            let newContact = await ContactModel.readMoreContacts(currentUserId, skipNumberContacts, LIMIT_NUMBER);

            let users = newContact.map(async(contact) => {
                if (contact.contactId == currentUserId) {
                    return await UserModel.getNormalUserDataById(contact.userId);
                } else {
                    return await UserModel.getNormalUserDataById(contact.contactId);
                }
            });
            // console.log(users);
            resolve(await Promise.all(users));
        } catch (error) {
            reject(error)
        };
    });
};
/**
 * read more sent contact
 * @param {_id-string} currentUserId
 * @param {number} skipNumberContacts
 * @returns
 */
const readMoreContactSentService = (currentUserId, skipNumberContacts) => {
    return new Promise(async(resolve, reject) => {
        try {
            let newContact = await ContactModel.readMoreContactsSent(currentUserId, skipNumberContacts, LIMIT_NUMBER);

            let users = newContact.map(async(contact) => {
                return await UserModel.getNormalUserDataById(contact.contactId);
            });
            // console.log(users);
            resolve(await Promise.all(users));
        } catch (error) {
            reject(error)
        };
    });
};
const readMoreContactReceivedService = (currentUserId, skipNumberContacts) => {
    return new Promise(async(resolve, reject) => {
        try {
            let newContact = await ContactModel.readMoreContactsReceived(currentUserId, skipNumberContacts, LIMIT_NUMBER);

            let users = newContact.map(async(contact) => {
                return await UserModel.getNormalUserDataById(contact.userId);
            });
            // console.log(users);
            resolve(await Promise.all(users));
        } catch (error) {
            reject(error)
        };
    });
};
const searchFriendsService = (currentUserId, keyword) => {
    return new Promise(async(resolve, reject) => {
        let friendsId = [];
        let friends = await ContactModel.getFriend(currentUserId);
        friends.forEach((item)=>{
        friendsId.push(item.userId);
        friendsId.push(item.contactId);
        });
        friendsId = _.uniqBy(friendsId);
        friendsId = friendsId.filter(userID =>{
            return userID != currentUserId;
        });
        let users = await UserModel.fillAllToAddGroupChat(friendsId, keyword);
        resolve(users);

    });
};
export {
    findUserContactService,
    addNewContactService,
    getContactServices,
    getContactReceivedServices,
    getContactSentServices,
    countAllContactsReceivedService,
    countAllContactsService,
    countAllContactsSentService,
    readMoreContactService,
    readMoreContactSentService,
    readMoreContactReceivedService,
    removeReqContactSentService,
    removeReqContactReceivedService,
    approveReqContactReceivedService,
    removeContactContactService,
    searchFriendsService
};