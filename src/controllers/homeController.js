import { getNotificationsService, countNotifUnread } from "../services/notificationService.js";
import { getContactReceivedServices, getContactServices, getContactSentServices, countAllContactsReceivedService, countAllContactsService, countAllContactsSentService } from "../services/contactService.js";
import { getAllConversationItemService } from "../services/messageService.js";
import { bufferToBase64,lastItemOfArray, convertTimestamp } from "../helpers/clientHelper.js";
import request from "request";

const getICETurnServer = () => {
  return new Promise(async (resolve, reject) => {
    // Node Get ICE STUN and TURN list
    // let o = {
    //   format: "urls",
    // };
    // let bodyString = JSON.stringify(o);
    // let options = {
    //     url: "https://global.xirsys.net/_turn/MyFirstApp",
    // //   host: "global.xirsys.net",
    // //   path: "/_turn/MyFirstApp",
    //   method: "PUT",
    //   headers: {
    //     Authorization:
    //       "Basic " +
    //       Buffer.from(
    //         "cuongnguyen2202:05c56938-b0c7-11ec-9293-0242ac130003"
    //       ).toString("base64"),
    //     "Content-Type": "application/json",
    //     "Content-Length": bodyString.length,
    //   },
    // };
    // // call a request to get ICE list turn server
    // request(options, (error, response, body) =>{
    //     if(error){
    //         console.log("ICE list error",error);
    //         return reject(err);
    //     }
    //  let bodyJSON = JSON.parse(body);
    //  resolve(bodyJSON.v.iceServers)
    // });


       resolve([]);
  });
};

const getHome = async(req, res) => {
    // 10 item 1 time
    let notifications = await getNotificationsService(req.user._id);
    // get amount notif unread
    let countNotiUnread = await countNotifUnread(req.user._id);
    // get contact 10 item 1 time
    let contacts = await getContactServices(req.user._id);
    // get contact send 10 item 1 time
    let contactsSent = await getContactSentServices(req.user._id);
    // get contact 10 receiver  item 1 time
    let contactsReceived = await getContactReceivedServices(req.user._id);
    //count contact
    let countAllContacts = await countAllContactsService(req.user._id);
    let countAllContactsSent = await countAllContactsSentService(req.user._id);
    let countAllContactsReceived = await countAllContactsReceivedService(req.user._id);
    let getAllConversations = await getAllConversationItemService(req.user._id);
    let allConversationsWithMessages = getAllConversations.allConversationsWithMessages;

    //get ICE List from xirsys turn server

    let iceTurnServer = await getICETurnServer();


    return res.render("main/home/home", {
        errors: req.flash("errors"),
        success: req.flash("success"),
        user: req.user,
        notifications: notifications,
        countNotiUnread: countNotiUnread,
        contacts: contacts,
        contactsSent: contactsSent,
        contactsReceived: contactsReceived,
        countAllContacts: countAllContacts,
        countAllContactsSent: countAllContactsSent,
        countAllContactsReceived: countAllContactsReceived,
        allConversationsWithMessages:allConversationsWithMessages,
        bufferToBase64: bufferToBase64,
        lastItemOfArray: lastItemOfArray,
        convertTimestamp:convertTimestamp,
        iceTurnServer: JSON.stringify(iceTurnServer)


    });
};
export { getHome };