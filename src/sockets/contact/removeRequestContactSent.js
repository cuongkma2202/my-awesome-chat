import { pushSocketIdToArray, emitNotifyToArray, removeSocketIdFromArray } from "../../helpers/socketHelper.js";
/**
 *
 * @param io from socket.io
 */
const removeRequestContactSent = (io) => {
    let clients = {};
    io.on("connection", (socket) => {
        clients = pushSocketIdToArray(clients, socket.request.user._id, socket.id);
        socket.on("remove-request-contact-sent", (data) => {
            let currentUser = {
                id: socket.request.user._id,
            };
            if (clients[data.contactId]) {

                emitNotifyToArray(clients, data.contactId, io, "response-remove-request-contact-sent", currentUser);
            };
            // io.emit("response-add-new-contact", currentUser);

        });
        socket.on("disconnect", () => {
            /*
            clients[currentUserId] = clients[currentUserId].filter((socketId) => {
              return socketId !== socket.id;
            });
            if (!clients[currentUserId].length) {
              delete clients[currentUserId];
            } */
            clients = removeSocketIdFromArray(clients, socket.request.user._id, socket);
        });
    });
};
export default removeRequestContactSent;