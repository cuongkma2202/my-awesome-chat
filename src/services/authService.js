import User from "../models/userModel.js";
import bcrypt from "bcrypt";
import { transError, transSuccess, transMail } from "../../lang/vi.js";
import { v4 as uuidv4 } from "uuid";
import sendMail from "../config/mailler.js";

const registerService = (email, gender, password, protocol, host) => {
    return new Promise(async(resolve, reject) => {
        let checkUser = await User.findByEmail(email);
        if (checkUser) {
            if (!checkUser.local.isActive) {
                return reject(transError.not_active);
            }
            return reject(transError.email_in_use);
        }
        const hashedPassword = await bcrypt.hash(
            password,
            parseInt(process.env.SALT_ROUND)
        );

        let userItem = {
            username: email.split("@")[0],
            gender: gender,
            local: {
                email: email,
                password: hashedPassword,
                verifyToken: uuidv4(),
            },
        };
        let user = await User.createNew(userItem);
        let linkVerify = `${protocol}://${host}/verify/${user.local.verifyToken}`;
        // send email
        sendMail(email, transMail.subject, transMail.template(linkVerify, email))
            .then((success) => {
                resolve(transSuccess.userCreated(user.local.email));
            })
            .catch(async(error) => {
                // remove
                await User.removeById(user._id);
                console.log(error);
                reject(transMail.send_fail);
            });
        // resolve(transSuccess.userCreated(user.local.email));
    });
};
const verifyAccount = (token) => {
    return new Promise(async(resolve, reject) => {
        const userByToken = await User.findByToken(token);
        if (!userByToken) {
            return reject(transError.token_null);
        }
        await User.verify(token);
        // console.log(user);
        resolve(transSuccess.account_activated);
    });
};
export { registerService, verifyAccount };