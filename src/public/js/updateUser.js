let userAvatar = null;
let userInfo = {};
let originAvatarSrc = null;
let originUserInfo = {};
let userUpdatePassword = {};
function callLogout() {
  let timerInterval;
  Swal.fire({
    position: "top-end",
    type: "success",
    title: "Logout in 5 second",
    html: "Time: <strong></strong>",
    showConfirmButton: false,
    timer: 5000,
    onBeforeOpen: () => {
      Swal.showLoading();
      timerInterval = setInterval(() => {
        Swal.getContent().querySelector("strong").textContent = Math.ceil(Swal.getTimerLeft() / 1000);
      }, 1000);
    },
    onClose: () => {
      clearInterval(timerInterval);
    }
  }).then((result) => {
    $.get("/logout", function () {
      location.reload();
    })
  })
}
function updateUserInfo() {
  $("#input-change-avatar").bind("change", function () {
    const fileData = $(this).prop("files")[0];
    const math = ["img/png", "image/jpg", "image/jpeg"];
    const limit = 1048576; // byte = 1mb
    if ($.inArray(fileData.type, math) === -1) {
      alertify.notify("File does not support. JPG, PNG only.", "error", 7);
      $(this).val(null);
      return false;
    }
    if (fileData.size > limit) {
      alertify.notify("Upload image no larger than 1MB", "error", 7);
      $(this).val(null);
      return false;
    }
    if (typeof FileReader != "undefined") {
      let imagePreview = $("#image-edit-profile");
      imagePreview.empty();
      let fileReader = new FileReader();
      fileReader.onload = function (element) {
        $("<img>", {
          "src": element.target.result,
          "class": "avatar img-circle",
          "id": "user-modal-avatar",
          "alt": "avatar",
        }).appendTo(imagePreview);
      };
      imagePreview.show();
      fileReader.readAsDataURL(fileData);
      let formData = new FormData();
      formData.append("avatar", fileData);

      userAvatar = formData
    } else {
      alertify.notify("FileReader dose not Support", "error", 7);
    }
  });
  $("#input-change-username").bind("change", function () {
    let username = $(this).val();
    let regexUsername = new RegExp(/^[\s0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/);
    if (!regexUsername.test(username) || username.length < 3 || username.length > 15) {
      alertify.notify("This field must be maximum length of 15 and no special characters", "error", 7);
      $(this).val(originUserInfo.username);
      delete userInfo.username;
      return false;
    }
    userInfo.username = username;
  });
  $("#input-change-gender-male").bind("click", function () {
    let gender = $(this).val();
    if (gender !== "male") {
      alertify.notify("Only accepted Male and Female. LGBT deny", "error", 7);
      $(this).val(originUserInfo.gender);
      delete userInfo.gender;
      return false;
    }

    userInfo.gender = gender;
  });
  $("#input-change-gender-female").bind("click", function () {
    let gender = $(this).val();
    if (gender !== "female") {
      alertify.notify("Only accepted Male and Female. LGBT deny", "error", 7);
      $(this).val(originUserInfo.gender);
      delete userInfo.gender;
      return false;
    }

    userInfo.gender = gender;
  });
  $("#input-change-address").bind("change", function () {
    let address = $(this).val();
    if (address.length < 3 || address.length > 30) {
      alertify.notify("This field must be maximum length of 30 and no special characters", "error", 7);
      $(this).val(originUserInfo.address);
      delete userInfo.address;
      return false;
    }

    userInfo.address = address;
  });
  $("#input-change-phone").bind("change", function () {
    let phone = $(this).val();
    let regexPhone = new RegExp(/^(0)[0-9]{9,10}$/);

    if (!regexPhone.test(phone) || phone.length < 9 || phone.length > 11) {
      alertify.notify("Invalid phone number", "error", 7);
      // console.log("Hello");
      $(this).val(originUserInfo.phone);
      delete userInfo.phone;
      return false;
    }


    userInfo.phone = phone;
  });
  $("#input-change-current-password").bind("change", function () {
    let currentPassword = $(this).val();
    let regexPassword = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/);

    if (!regexPassword.test(currentPassword)) {
      alertify.notify("Minium of different classes of characters in password is 8. Classes of character: digits, lowercase characters and special characters.", "error", 7);
      $(this).val(null);
      delete userUpdatePassword.currentPassword;
      return false;
    }
    userUpdatePassword.currentPassword = currentPassword;
  });
  $("#input-change-new-password").bind("change", function () {
    let newPassword = $(this).val();
    let regexPassword = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/);

    if (!regexPassword.test(newPassword)) {
      alertify.notify("Minium of different classes of characters in password is 8. Classes of character: digits, lowercase characters and special characters.", "error", 7);
      $(this).val(null);
      delete userUpdatePassword.newPassword;
      return false;
    }
    userUpdatePassword.newPassword = newPassword;
  });
  $("#input-change-confirm-new-password").bind("change", function () {
    let confirmNewPassword = $(this).val();
    if (!userUpdatePassword.newPassword) {
      alertify.notify("You must provide new password", "error", 7);
      $(this).val(null);
      delete userUpdatePassword.confirmNewPassword;
      return false;
    }
    if (confirmNewPassword !== userUpdatePassword.newPassword) {
      alertify.notify("Confirm password dose not match", "error", 7);
      $(this).val(null);
      delete userUpdatePassword.confirmNewPassword;
      return false;
    }
    userUpdatePassword.confirmNewPassword = confirmNewPassword;
  });

}
function callUpdateUserAvatar() {
  $.ajax({
    url: "/user/update-avatar",
    method: "put",
    cache: false,
    contentType: false,
    processData: false,
    data: userAvatar,
    success: function (result) {
      // console.log(result);
      // display success
      $(".user-modal-alert-success").find("span").text(result.message);
      $(".user-modal-alert-success").css("display", "block");
      //  update avatar navbar
      $("#navbar-avatar").attr("src", result.imageSrc);
      originAvatarSrc = result.imageSrc;
      // reset all
      $("#input-btn-cancel-update-user").click();
    },
    error: function (error) {
      // display error
      $(".user-modal-alert-error").find("span").text(error.responseText);
      $(".user-modal-alert-error").css("display", "block");
      // reset all
      $("#input-btn-cancel-update-user").click();

    }
  })
}
function callUpdateUserInfo() {
  $.ajax({
    url: "/user/update-info",
    method: "put",
    data: userInfo,
    success: function (result) {
      // console.log(result);
      // display success
      $(".user-modal-alert-success").find("span").text(result.message);
      $(".user-modal-alert-success").css("display", "block");
      // update originUserInfo
      originUserInfo = Object.assign(originUserInfo, userInfo);
      // upDateUsername navbar
      $("#navbar-username").text(originUserInfo.username);
      // reset all
      $("#input-btn-cancel-update-user").click();
    },
    error: function (error) {
      // display error
      $(".user-modal-alert-error").find("span").text(error.responseText);
      $(".user-modal-alert-error").css("display", "block");
      // reset all
      $("#input-btn-cancel-update-user").click();

    }
  })
}
function callUpdateUserPassword() {
  $.ajax({
    url: "/user/update-password",
    method: "put",
    data: userUpdatePassword,
    success: function (result) {
      // console.log(result);
      // display success
      $(".user-modal-password-alert-success").find("span").text(result.message);
      $(".user-modal-password-alert-success").css("display", "block");
      // update originUserInfo

      // reset all
      $("#input-btn-cancel-update-user-password").click();
      // logout after change password
      callLogout();
    },
    error: function (error) {
      // display error
      $(".user-modal-password-alert-error").find("span").text(error.responseText);
      $(".user-modal-password-alert-error").css("display", "block");
      // reset all
      $("#input-btn-cancel-update-user-password").click();

    }
  })
}
$(document).ready(function () {
  originAvatarSrc = $("#user-modal-avatar").attr("src");
  originUserInfo = {
    username: $("#input-change-username").val(),
    gender: ($("#input-change-gender-male").is(":checked")) ? $("#input-change-gender-male").val() : $("#input-change-gender-female").val(),
    address: $("#input-change-address").val(),
    phone: $("#input-change-phone").val(),
  }
  // update userInfo after change
  updateUserInfo();
  $("#input-btn-update-user").bind("click", function () {
    if ($.isEmptyObject(userInfo) && !userAvatar) {
      alertify.notify("You must change profile before update", "error", 7);
      return false
    }
    if (userAvatar) {
      callUpdateUserAvatar();
    }
    if (!$.isEmptyObject(userInfo)) {
      callUpdateUserInfo();
    }

  });
  $("#input-btn-cancel-update-user").bind("click", function () {
    userAvatar = null;
    userInfo = {};
    $("#input-change-avatar").val(null);
    $("#user-modal-avatar").attr("src", originAvatarSrc);
    $("#input-change-username").val(originUserInfo.username);
    (originUserInfo.gender === "male") ? $("#input-change-gender-male").click() : $("#input-change-gender-female").click();
    $("#input-change-address").val(originUserInfo.address);
    $("#input-change-phone").val(originUserInfo.phone);

  });
  $("#input-btn-update-user-password").bind("click", function () {
    if (!userUpdatePassword.currentPassword || !userUpdatePassword.newPassword || !userUpdatePassword.confirmNewPassword) {
      alertify.notify("Please complete all field before update ", "error", 7);
      return false;
    }
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, Update it!"
    }).then((result) => {
      if (!result.value) {
        $("#input-btn-cancel-update-user-password").click();
        return false;
      }
      callUpdateUserPassword();
    });

  });
  $("#input-btn-cancel-update-user-password").bind("click", function () {
    userUpdatePassword = {};
    $("#input-change-current-password").val(null);
    $("#input-change-new-password").val(null);
    $("#input-change-confirm-new-password").val(null);

  });

});
