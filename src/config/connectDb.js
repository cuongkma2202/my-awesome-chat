import mongoose from "mongoose";
import bluebird from "bluebird";
// Connect to mongodb
const connectToDb = () => {
  mongoose.Promise = bluebird;
  const URI = `${process.env.DB_CONNECTION}://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;
  return mongoose.connect(URI, { useNewUrlParser: true });
};
export default connectToDb;
