import passport from "passport";
import passportGoogle from "passport-google-oauth20";
import User from "../../models/userModel.js";
import { transError, transSuccess } from "../../../lang/vi.js";
import { env } from "../../config/environment.js";
import ChatGroupModel from "../../models/chatGroupModel.js"

const GoogleStrategy = passportGoogle.Strategy;
/**
 * Valid user account type facebook
 */
let initPassportGoogle = () => {
    passport.use(
        new GoogleStrategy({
                clientID: env.GG_APP_ID,
                clientSecret: env.GG_APP_SECRET,
                callbackURL: env.GG_CALLBACK_URL,
                passReqToCallback: true,
            },
            async(req, accessToken, refreshToken, profile, done) => {
                try {
                    const user = await User.findByGoogleUid(profile.id);
                    if (user) {
                        return done(
                            null,
                            user,
                            req.flash("success", transSuccess.loginSuccess(user.username))
                        );
                    }

                    let newUserItem = {
                        username: profile.displayName,
                        gender: profile.gender,
                        local: {
                            isActive: true,
                        },
                        google: {
                            uid: profile.id,
                            token: accessToken,
                            email: profile.emails[0].value,
                        },
                    };
                    const newUser = await User.createNew(newUserItem);
                    return done(
                        null,
                        newUser,
                        req.flash("success", transSuccess.loginSuccess(newUser.username))
                    );
                } catch (error) {
                    console.log(error);
                    return done(
                        null,
                        false,
                        req.flash("errors", transError.server_error)
                    );
                }
            }
        )
    );
    // Save userID to Session
    passport.serializeUser((user, done) => {
        done(null, user._id);
    });
    passport.deserializeUser(async(id, done) => {
        try {
            let user = await User.findUserByIdForSessionToUse(id);
            let getChatGroupIds = await ChatGroupModel.getChatGroupIdsByUser(user._id);

            user = user.toObject();
            user.chatGroupIds = getChatGroupIds;

            return done(null, user);
          } catch (error) {
            return done(error, null);
          }
    });
};
export { initPassportGoogle };