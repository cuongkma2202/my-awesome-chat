import addNewContact from "./contact/addNewContact.js";
import removeRequestContactSent from "./contact/removeRequestContactSent.js";
import removeRequestContactReceived from "./contact/removeRequestContactReceived.js";
import approveRequestContactReceived from "./contact/approveRequestContactReceived.js";
import removeContact from './contact/removeContact.js';
import chatTextEmoji from "./chat/chatTextEmojiSocket.js";
import typingOn from "./chat/typingOn.js";
import typingOff from "./chat/typingOff.js";
import imageChatSocket from "./chat/imageChatSocket.js";
import attachmentChatSocket from "./chat/attachmentChatSocket.js";
import videoCallSocket from "./chat/videoCallSocket.js";
import userOnlineOfflineSocket from "./status/userOnlineOffline.js";
import newGroupChatSocket from "./group/newGroupChat.js";

/**
 *
 * @param io socket.io
 */
const initSockets = (io) => {
    addNewContact(io);
    removeRequestContactSent(io);
    removeRequestContactReceived(io);
    approveRequestContactReceived(io);
    removeContact(io);
    chatTextEmoji(io);
    typingOn(io);
    typingOff(io);
    imageChatSocket(io);
    attachmentChatSocket(io);
    videoCallSocket(io);
    userOnlineOfflineSocket(io);
    newGroupChatSocket(io);


}

export default initSockets;