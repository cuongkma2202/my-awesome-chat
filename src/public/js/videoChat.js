function videoChat(divId){
$(`#video-chat-${divId}`).unbind("click").on("click", function(){
  let targetId = $(this).data("chat");
  let callerName = $("#navbar-username").text();
  let dataToEmit = {
    listenerId : targetId,
    callerName: callerName
  };
  // 1: caller - check user listen online
  socket.emit("caller-check-listener-online", dataToEmit);
});
};

function playVideoStream(videoTagId, stream){
let video = document.getElementById(videoTagId);
video.srcObject = stream;
video.onloadeddata = function(){
  video.play();
}
};
function closeVideoStream(stream){
  return stream.getTracks().forEach(track => {
    return track.stop();
  })
}

$(document).ready(function(){
// 2: of caller
socket.on("server-send-listener-is-offline", function(){
  alertify.notify("User is offline", "error", 5);
});
let iceServerList = $("#ice-server-list").val();
// console.log(JSON.parse(iceServerList));
let getPeerId = "";
const peer = new Peer({
  key: "peerjs",
  host: "peerjs-server-trungquandev.herokuapp.com",
  secure: true,
  port: 443,
  debug: 0,
  config: { 'iceServers': iceServerList }
});
// console.log(peer);
peer.on("open", function(peerId){
  getPeerId = peerId;
});

// 3. of listener
socket.on("server-req-peerId-of-listener", function(response){
let listenerName = $("#navbar-username").text();
let dataToEmit = {
  callerId : response.callerId,
  callerName: response.callerName,
  listenerId: response.listenerId,
  listenerPeerId : getPeerId,
  listenerName: listenerName
};
// 4. of listener - send peerId to server
socket.emit("listener-emit-peerId-to-server", dataToEmit);
});
//5. of caller
let timerInterval;
socket.on("server-send-peerId-of-listener-to-caller", function(response){
  let dataToEmit = {
    callerId : response.callerId,
    callerName: response.callerName,
    listenerId: response.listenerId,
    listenerPeerId :response.listenerPeerId,
    listenerName: response.listenerName
  };

  //6. of caller
  socket.emit("caller-request-call-to-server", dataToEmit);


  Swal.fire({
    type: "success",
    title: `Calling to &nbsp; <span style="color: #2ecc71;">${response.listenerName}</span> &nbsp; <i class="fa fa-volume-control-phone"></i>`,
    html: `
    Time: <strong style="color: #d43f3a;"></strong> s. <br/> <br/>
    <button id="btn-cancel-call" class="btn btn-danger"> Cancel </button>
    `,
    backdrop: "rgba(85,85,85,0.4)" ,
    with: "52rem",
    allowOutsideClick: false,
    timer: 30000,
    onBeforeOpen: () => {
      $("#btn-cancel-call").unbind("click").on("click", function(){
        Swal.close();
        clearInterval(timerInterval);

        //7. of caller cancel call while calling
        socket.emit("caller-cancel-request-call-to-server", dataToEmit);

      });
      if(Swal.getContent().querySelector!== null){
        Swal.showLoading();
        timerInterval = setInterval(() => {
          Swal.getContent().querySelector("strong").textContent = Math.ceil(Swal.getTimerLeft() / 1000);
        }, 1000);
      }

    },
    onOpen: () => {
    //12. of caller
    socket.on("server-send-reject-call-to-caller", function(response){
      Swal.close();
      clearInterval(timerInterval);
      Swal.fire({
       type: "info",
       title: `<span style="color: #2ecc71;">${response.listenerName}</span> not responding. Please call later`,
       backdrop: "rgba(85,85,85,0.4)" ,
      with: "52rem",
      allowOutsideClick: false,
      confirmButtonColor: "#2ECC71",
      confirmButtonText: "Close"
      });

    });

    },
    onClose: () => {
      clearInterval(timerInterval);
    }
  }).then((result) => {
        return false;
    })
  });

//8. of listener
socket.on("server-send-request-call-to-listener", function(response){
  let dataToEmit = {
    callerId : response.callerId,
    callerName: response.callerName,
    listenerId: response.listenerId,
    listenerPeerId :response.listenerPeerId,
    listenerName: response.listenerName
  };
  Swal.fire({
    type: "success",
    title: `<span style="color: #2ecc71;">${response.callerName}</span> is calling &nbsp; <i class="fa fa-volume-control-phone"></i>`,
    html: `
    Time: <strong style="color: #d43f3a;"></strong> s. <br/> <br/>
    <button id="btn-reject-call" class="btn btn-danger"> Deny </button>

    <button id="btn-accept-call" class="btn btn-success"> Accept </button>
    `,
    backdrop: "rgba(85,85,85,0.4)" ,
    with: "52rem",
    allowOutsideClick: false,
    timer: 30000,
    onBeforeOpen: () => {
      $("#btn-reject-call").unbind("click").on("click", function(){
        Swal.close();
        clearInterval(timerInterval);

        //10. of listener- reject call
        socket.emit("listener-reject-request-call-to-server", dataToEmit);
      });
      $("#btn-accept-call").unbind("click").on("click", function(){
        Swal.close();
        clearInterval(timerInterval);

        //11. of listener- reject call
        socket.emit("listener-accept-request-call-to-server", dataToEmit);
      });
      if(Swal.getContent().querySelector!== null){
        Swal.showLoading();
        timerInterval = setInterval(() => {
          Swal.getContent().querySelector("strong").textContent = Math.ceil(Swal.getTimerLeft() / 1000);
        }, 1000);

      }

    },
    onOpen: () =>{
       //9. of listener

       socket.on("server-send-cancel-request-call-to-listener", function(response){
        Swal.close();
        clearInterval(timerInterval);
       });
        //14. of listener

    },
    onClose: () => {
      clearInterval(timerInterval);
    }
  }).then((result) => {
        return false;
    });
});


//13. caller ok

socket.on("server-send-accept-call-to-caller", function(response) {
  Swal.close();
  clearInterval(timerInterval);

  let getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia).bind(navigator);

  getUserMedia({video: true, audio: true}, function(stream) {
    // Show modal streaming
    $("#streamModal").modal("show");

    // Play my stream in local (of caller)
    playVideoStream("local-stream", stream);

    // call to listener
    let call = peer.call(response.listenerPeerId, stream);

    // listen & play stream of listener
    call.on("stream", function(remoteStream) {
      // Play stream of listener)
      playVideoStream("remote-stream", remoteStream);
    });

    // Close modal: remove stream
    $("#streamModal").on("hidden.bs.modal", function() {
      closeVideoStream(stream);
      Swal.fire({
        type: "info",
        title: `Đã kết thúc cuộc gọi với &nbsp; <span style="color: #2ECC71;">${response.listenerName}</span>`,
        backdrop: "rgba(85, 85, 85, 0.4)",
        width: "52rem",
        allowOutsideClick: false,
        confirmButtonColor: "#2ECC71",
        confirmButtonText: "Close"
      });
    });
  }, function(err) {
    if (err.toString() === "NotAllowedError: Permission denied") {
      alertify.notify("Xin lỗi, bạn đã tắt quyền truy cập vào thiết bị nghe gọi trên trình duyệt, vui lòng mở lại trong phần cài đặt của trình duyệt.", "error", 7);
    }
    if (err.toString() === "NotFoundError: Requested device not found") {
      alertify.notify("Xin lỗi, chúng tôi không tìm thấy thiết bị nghe gọi trên máy tính của bạn.", "error", 7);
    }
  });
});
// 14
socket.on("server-send-accept-call-to-listener", function(response) {
  Swal.close();
  clearInterval(timerInterval);
  console.log("ok");

  let getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia).bind(navigator);

  peer.on("call", function(call) {
    getUserMedia({video: true, audio: true}, function(stream) {
      // Show modal streaming
      $("#streamModal").modal("show");

      // Play my stream in local (of listener)
      playVideoStream("local-stream", stream);

      call.answer(stream); // Answer the call with an A/V stream.

      call.on("stream", function(remoteStream) {
        // Play stream of caller)
        playVideoStream("remote-stream", remoteStream);
      });

      // Close modal: remove stream
      $("#streamModal").on("hidden.bs.modal", function() {
        closeVideoStream(stream);
        Swal.fire({
          type: "info",
          title: `Ended call &nbsp; <span style="color: #2ECC71;">${response.callerName}</span>`,
          backdrop: "rgba(85, 85, 85, 0.4)",
          width: "52rem",
          allowOutsideClick: false,
          confirmButtonColor: "#2ECC71",
          confirmButtonText: "Close"
        });
      });
    }, function(err) {
      console.log(err);
      if (err.toString() === "NotAllowedError: Permission denied") {
        alertify.notify("Xin lỗi, bạn đã tắt quyền truy cập vào thiết bị nghe gọi trên trình duyệt, vui lòng mở lại trong phần cài đặt của trình duyệt.", "error", 7);
      };
      if (err.toString() === "NotFoundError: Requested device not found") {
        alertify.notify("Xin lỗi, chúng tôi không tìm thấy thiết bị nghe gọi trên máy tính của bạn.", "error", 7);
      };
    });

  });
});


});