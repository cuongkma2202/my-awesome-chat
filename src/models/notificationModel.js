import mongoose from "mongoose";
const Schema = mongoose.Schema;

const NotificationSchema = new Schema({
    senderId: String,
    receiverId: String,
    type: String,
    isRead: { type: Boolean, default: false },
}, { timestamps: true });
NotificationSchema.statics = {
    createNew(item) {
        return this.create(item);
    },
    removeReqContactSentNotification(senderId, receiverId, type) {
        return this.deleteOne({
            $and: [
                { "senderId": senderId },
                { "receiverId": receiverId },
                { "type": type }

            ]
        }).exec();
    },
    /**
     * limit show notif
     * @param {string} userId
     * @param {number} limit
     */
    getByUserIdLimit(userId, limit) {
        return this.find({
            "receiverId": userId
        }).sort({ "createdAt": -1 }).limit(limit).exec();
    },

    countNotifUnread(userId) {
        return this.count({
            $and: [
                { "receiverId": userId },
                { "isRead": false }
            ]
        }).exec();
    },
    readMore(userId, skip, limit) {
        return this.find({
            "receiverId": userId
        }).sort({ "createdAt": -1 }).skip(skip).limit(limit).exec();
    },
    /**
     *
     * @param {*} userId
     * @param {arr} targetUsers
     */
    markAllAsRead(userId, targetUsers) {
        return this.updateMany({
            $and: [
                { "receiverId": userId },
                { "senderId": { $in: targetUsers } }
            ]
        }, { "isRead": true }).exec();
    }
}
const NOTIFICATION_TYPES = {
    ADD_CONTACT: "add_contact",
    APPROVE_CONTACT: "approve_contact"
}
const NOTIFICATION_CONTENT = {
    getContent: (notificationType, isRead, userId, username, userAvatar) => {
        if (notificationType === NOTIFICATION_TYPES.ADD_CONTACT) {
            if (!isRead) {
                return `<div data-uid="${userId}" class="notif-readed-false">
                    <img
                      class="avatar-small"
                      src="images/users/${userAvatar}"
                      alt=""
                    />
                    <strong>${username}</strong> send you friend request!</div>`
            }
            return `<div data-uid="${userId}">
                    <img
                      class="avatar-small"
                      src="images/users/${userAvatar}"
                      alt=""
                    />
                    <strong>${username}</strong> send you friend request! </div>`
        };
        if (notificationType === NOTIFICATION_TYPES.APPROVE_CONTACT) {
            if (!isRead) {
                return `<div data-uid="${userId}" class="notif-readed-false">
                    <img
                      class="avatar-small"
                      src="images/users/${userAvatar}"
                      alt=""
                    />
                    <strong>${username}</strong> approve your friend request!</div>`
            }
            return `<div data-uid="${userId}">
                    <img
                      class="avatar-small"
                      src="images/users/${userAvatar}"
                      alt=""
                    />
                    <strong>${username}</strong> approve you friend request! </div>`
        }
        return "No matching with any notification type";
    },
}

const NotificationModel = mongoose.model("notification", NotificationSchema);
export default { NotificationModel, NOTIFICATION_TYPES, NOTIFICATION_CONTENT };