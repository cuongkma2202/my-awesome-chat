import { Schema, model } from "mongoose";

const ContactSchema = new Schema(
  {
    userId: String,
    contactId: String,
    status: { type: Boolean, default: false },
  },

  { timestamps: true }
);
module.exports = model("contact", ContactSchema);
