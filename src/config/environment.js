import dotenv from "dotenv";
dotenv.config();

const env = {
    DB_CONNECTION: process.env.DB_CONNECTION,
    DB_HOST: process.env.DB_HOST,
    DB_PORT: process.env.DB_PORT,
    DB_NAME: process.env.DB_NAME,
    DB_USERNAME: process.env.DB_USERNAME,
    DB_PASSWORD: process.env.DB_PASSWORD,
    HOSTNAME: process.env.HOSTNAME,
    PORT: process.env.PORT,
    SESSION_KEY: process.env.SESSION_KEY,
    SESSION_SECRET: process.env.SESSION_SECRET,
    SALT_ROUND: process.env.SALT_ROUND,
    MAIL_USER: process.env.MAIL_USER,
    MAIL_PASSWORD: process.env.MAIL_PASSWORD,
    MAIL_HOST: process.env.MAIL_HOST,
    MAIL_PORT: process.env.MAIL_PORT,
    // config facebook login app
    FACEBOOK_APP_ID: process.env.FACEBOOK_APP_ID,
    FACEBOOK_APP_SECRET: process.env.FACEBOOK_APP_SECRET,
    FACEBOOK_CALLBACK_URL: process.env.FACEBOOK_CALLBACK_URL,
    GG_APP_ID: process.env.GG_APP_ID,
    GG_APP_SECRET: process.env.GG_APP_SECRET,
    GG_CALLBACK_URL: process.env.GG_CALLBACK_URL,
};
export { env };