import { Schema, model } from "mongoose";

const MessageSchema = new Schema(
  {
    userId: String,
    contactId: String,
    status: { type: Boolean, default: false },
  },

  { timestamps: true }
);
module.exports = model("Message", MessageSchema);
