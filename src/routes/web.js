import express from "express";
import { initPassportLocal } from "../controllers/passportController/local.js";
import { initPassportFacebook } from "../controllers/passportController/facebook.js";
import { initPassportGoogle } from "../controllers/passportController/google.js";
import {
    getLoginRegister,
    postRegister,
    verifyAcc,
    getLogout,
    checkLoggedIn,
    checkLoggedOut,
} from "../controllers/authController.js";
import { getHome } from "../controllers/homeController.js";
import { register, } from "../validation/authValidation.js";
import { updatePassword, userValidation } from "../validation/userValidation.js";
import {checkMessageLength} from "../validation/messagesValidation.js"
import passport from "passport";
import {
    updateAvatar,
    updateUserInformation,
    userUpdatePassword
} from "../controllers/userController.js";
import {
    addNewContact,
    findUserContact,
    readMoreContacts,
    removeRequestContactSent,
    removeRequestContactReceived,
    readMoreContactsSent,
    readMoreContactsReceived,
    approveRequestContactReceived,
    removeContact,
    searchFriend
} from "../controllers/contactController.js";
import { contactFindUserValidate, searchFriendsValidate } from "../validation/contactValidation.js";
import { markAllAsRead, readMore } from "../controllers/notificationController.js";
import { addNewAttachmentMessage, addNewImageMessage, addNewTextEmoji, readMoreAllChat, readMoreGroupChat, readMoreMessage, readMorePersonalChat, searchConversation } from "../controllers/messagesController.js";
import { addNewGroupValidation } from "../validation/groupChatValidation.js";
import { addNewGroup } from "../controllers/groupChatController.js";
import {searchConversationValidation } from "../validation/newFeatureValidation.js";
// init password local
initPassportLocal();
// init passport facebook
initPassportFacebook();
// init passport google
initPassportGoogle();
const router = express.Router();

const initRoutes = (app) => {
    router.get("/login-register", checkLoggedOut, getLoginRegister);
    router.post("/register", checkLoggedOut, register, postRegister);
    router.get("/verify/:token", checkLoggedOut, verifyAcc);
    router.post(
        "/login",
        checkLoggedOut,
        passport.authenticate("local", {
            successRedirect: "/",
            failureRedirect: "/login-register",
            successFlash: true,
            failureFlash: true,
        })
    );
    router.get(
        "/auth/facebook",
        checkLoggedOut,
        passport.authenticate("facebook", {
            scope: ["email"],
        })
    );
    router.get(
        "/auth/facebook/callback",
        checkLoggedOut,
        passport.authenticate("facebook", {
            successRedirect: "/",
            failureRedirect: "/login-register",
        })
    );
    router.get(
        "/auth/google",
        checkLoggedOut,
        passport.authenticate("google", {
            scope: ["openid", "profile", "email"],
        })
    );
    router.get(
        "/auth/google/callback",
        checkLoggedOut,
        passport.authenticate("google", {
            successRedirect: "/",
            failureRedirect: "/login-register",
        })
    );

    router.get("/", checkLoggedIn, getHome);
    router.get("/logout", checkLoggedIn, getLogout);
    router.put("/user/update-avatar", checkLoggedIn, updateAvatar);
    router.put("/user/update-info", checkLoggedIn, userValidation, updateUserInformation);
    router.put("/user/update-password", checkLoggedIn, updatePassword, userUpdatePassword);

    router.get("/contact/find-users/:keyword", checkLoggedIn, contactFindUserValidate, findUserContact);
    router.post("/contact/add-new", checkLoggedIn, addNewContact);
    router.delete("/contact/remove-contact", checkLoggedIn, removeContact)
    router.delete("/contact/remove-request-contact-sent", checkLoggedIn, removeRequestContactSent);
    router.delete("/contact/remove-request-contact-received", checkLoggedIn, removeRequestContactReceived);
    router.put("/contact/approve-request-contact-received", checkLoggedIn, approveRequestContactReceived);
    router.get("/contact/read-more-contacts", checkLoggedIn, readMoreContacts);
    router.get("/contact/read-more-contact-sent", checkLoggedIn, readMoreContactsSent);
    router.get("/contact/read-more-contact-received", checkLoggedIn, readMoreContactsReceived);
    router.get("/contact/search-friends/:keyword", checkLoggedIn, searchFriendsValidate, searchFriend);

    router.get("/notification/read-more", checkLoggedIn, readMore);
    router.put("/notification/mark-all-as-read", checkLoggedIn, markAllAsRead);

    router.post("/message/add-new-text-emoji", checkLoggedIn, checkMessageLength, addNewTextEmoji);
    router.post("/message/add-new-image", checkLoggedIn,  addNewImageMessage);
    router.post("/message/add-new-attachment", checkLoggedIn,  addNewAttachmentMessage);
    router.get("/message/read-more-all-chat", checkLoggedIn, readMoreAllChat);
    router.get("/message/read-more-message", checkLoggedIn, readMoreMessage);


    router.post("/group-chat/add-new", checkLoggedIn, addNewGroupValidation, addNewGroup);
    router.get("/conversation/search/:keyword", checkLoggedIn, searchConversationValidation, searchConversation);
    router.get("/message/read-more-personal-chat", checkLoggedIn, readMorePersonalChat);
    router.get("/message/read-more-group-chat", checkLoggedIn, readMoreGroupChat);


    app.use("/", router);
};


export default initRoutes;