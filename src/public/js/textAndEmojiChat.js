function textAndEmojiChat(divId) {
  $(".emojionearea")
    .unbind("keyup")
    .on("keyup", function (element) {
      let currentEmojioneArea = $(this);
      if (element.which === 13) {
        let targetId = $(`#write-chat-${divId}`).data("chat");
        let messageVal = $(`#write-chat-${divId}`).val();
        if (!targetId || !messageVal.length) {
          return false;
        }
        let dataTextEmojiForSend = {
          uid: targetId,
          messageVal: messageVal,
        };
        if ($(`#write-chat-${divId}`).hasClass("chat-in-group")) {
          dataTextEmojiForSend.isChatGroup = true;
        }

        // call send message
        $.post(
          "/message/add-new-text-emoji",
          dataTextEmojiForSend,
          function (data) {
            let dataToEmit = {
              message: data.message,
            };
            console.log(data);
            // 1. handle message before send
            let divContent = $(`<div class="chat-content me" data-mess-id="${data.message._id}"></div>`)
            let messageOfMe = $(`
          <div class="convert-emoji bubble me" data-mess-id="${data.message._id}"></div>
          `);

           messageOfMe.text(data.message.text);

            let convertEmojiMessage = emojione.toImage(messageOfMe.html());
            messageOfMe = messageOfMe.html(convertEmojiMessage);
            console.log(messageOfMe);

            if (dataTextEmojiForSend.isChatGroup) {
              let userInfo = `<div class="infor-user-chat me">
              <p>${data.message.sender.name}</p>
              <img src="/images/users/${data.message.sender.avatar}" class="avatar-small" title="${data.message.sender.name}"/>
             </div>`
              divContent.html(`${userInfo}`).append(messageOfMe);
              increaseNumberMessageGroup(divId);
              dataToEmit.groupId = targetId;
            } else {
              messageOfMe.html(convertEmojiMessage);
              dataToEmit.contactId = targetId;
            }


            // 2. append message to screen
            if(dataTextEmojiForSend.isChatGroup){
              $(`.right .chat[data-chat=${divId}]`).append(divContent);
              nineScrollRight(divId);
            }else{
              $(`.right .chat[data-chat=${divId}]`).append(messageOfMe);
              nineScrollRight(divId)
            }
        ;

            // 3. remove input
            $(`#write-chat-${divId}`).val("");
            currentEmojioneArea.find(".emojionearea-editor").text("");

            // 4. change preview and timestamp in leftSide
            $(`.person[data-chat = ${divId}]`)
              .find("span.time").removeClass("message-realtime")
              .html(
                moment(data.message.createdAt)
                  .locale("en")
                  .startOf("seconds")
                  .fromNow()
              );
            $(`.person[data-chat = ${divId}]`)
              .find("span.preview")
              .html(emojione.toImage(data.message.text));

            // 5. move conversation to top
            $(`.person[data-chat = ${divId}]`).click(
              "triggerEvent.moveConversationToTop",
              function () {
                let dataToMove = $(this).parent();
                $(this).closest("ul").prepend(dataToMove);
                $(this).off("triggerEvent.moveConversationToTop");
              }
            );
            $(`.person[data-chat = ${divId}]`).click();

            // 6. emit realtime
            socket.emit("chat-text-emoji", dataToEmit);
            // 7.emit remove typing realtime
            typingOff(divId);
            // 8.if this has typing remove remove gif
            let checkTyping =  $(`.chat[data-chat=${divId}]`).find(`div.bubble-typing-gif`);
            if(checkTyping.length){
              checkTyping.remove();
            }
          }
        ).fail(function (res) {
          console.log(res);
          alertify.notify(res.responseText, "error", 7);
        });
      }
    });
}
$(document).ready(function () {
  socket.on("response-chat-text-emoji", function (response) {
    let divId = "";
    let divContent = $(`<div class="chat-content you" data-mess-id="${response.message._id}"></div>`)
    let messageOfYou = $(`
  <div class="convert-emoji bubble you" data-mess-id="${response.message._id}"></div>
  `);
    messageOfYou.text(response.message.text);
    let convertEmojiMessage = emojione.toImage(messageOfYou.html());
    messageOfYou = messageOfYou.html(convertEmojiMessage);
    if (response.currentGroupId) {
      divId = response.currentGroupId;
      let userInfo = `<div class="infor-user-chat you">
         <img src="/images/users/${response.message.sender.avatar}" class="avatar-small" title="${response.message.sender.name}"/>
         <p>${response.message.sender.name}</p>
        </div>
        `

        divContent.html(`${userInfo}`).append(messageOfYou);
      if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
        increaseNumberMessageGroup(divId);
      }

    } else {
      divId = response.currentUserId;
      messageOfYou.html(convertEmojiMessage);
    }
     console.log(divContent);
    // 2. append message to screen
    if(response.currentGroupId){
      if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
        $(`.right .chat[data-chat=${divId}]`).append(divContent);
        nineScrollRight(divId);
        $(`.person[data-chat = ${divId}]`)
        .find("span.time")
        .addClass("message-realtime")
      }
    }else{
      if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
        $(`.right .chat[data-chat=${divId}]`).append(messageOfYou);
        nineScrollRight(divId);
        $(`.person[data-chat = ${divId}]`)
        .find("span.time")
        .addClass("message-realtime")
      }
    }


    // 3. remove input nothing
    // 4. change preview and timestamp in leftSide
    $(`.person[data-chat = ${divId}]`)
      .find("span.time")
      .html(
        moment(response.message.createdAt)
          .locale("en")
          .startOf("seconds")
          .fromNow()
      );
    $(`.person[data-chat = ${divId}]`)
      .find("span.preview")
      .html(emojione.toImage(response.message.text));
    // 5. move conversation to top
    $(`.person[data-chat = ${divId}]`).click(
      "triggerEvent.moveConversationToTop",
      function () {
        let dataToMove = $(this).parent();
        $(this).closest("ul").prepend(dataToMove);
        $(this).off("triggerEvent.moveConversationToTop");
      }
    );
    $(`.person[data-chat = ${divId}]`).trigger("triggerEvent.moveConversationToTop");

 // 6. emit realtime

  });
});
