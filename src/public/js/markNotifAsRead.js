function markNotificationAsRead(targetUsers) {
  // console.log(targetUsers);
  $.ajax({
    url: "/notification/mark-all-as-read",
    method: "put",
    data: {
      targetUsers: targetUsers
    },
    success: function (result) {
      if (result) {
        targetUsers.forEach(function (uid) {
          $(".noti_content").find(`div[data-uid = ${uid}]`).removeClass("notif-readed-false");
          $("ul.list-notifications").find(`li>div[data-uid = ${uid}]`).removeClass("notif-readed-false");
        });
        decreaseNumberNotification("noti_counter", targetUsers.length);
      }
    }
  })
}
$(document).ready(function () {
  //navbar.ejs
  $("#popup-mark-notif-as-read").bind("click", function () {
    let targetUsers = [];
    $(".noti_content").find(".notif-readed-false").each(function (index, notification) {
      targetUsers.push($(notification).data("uid"));
    });
    if (!targetUsers.length) {
      alertify.notify("Readed all notification", "error", 7);
      return false;
    }
    markNotificationAsRead(targetUsers);
  });
  // modalnotification.ejs
  $("#modal-mark-notif-as-read").bind("click", function () {
    let targetUsers = [];
    $("ul.list-notifications").find("li>div.notif-readed-false").each(function (index, notification) {
      targetUsers.push($(notification).data("uid"));
    });
    if (!targetUsers.length) {
      alertify.notify("Readed all notification", "error", 7);
      return false;
    }
    markNotificationAsRead(targetUsers);
  });
})