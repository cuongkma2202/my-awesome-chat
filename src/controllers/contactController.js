import {
    addNewContactService,
    findUserContactService,
    removeReqContactSentService,
    removeReqContactReceivedService,
    readMoreContactService,
    readMoreContactSentService,
    readMoreContactReceivedService,
    approveReqContactReceivedService,
    removeContactContactService,
    searchFriendsService
} from "../services/contactService.js";
import { validationResult } from "express-validator";
const findUserContact = async(req, res) => {
    let errorArr = [];
    const validationError = validationResult(req);
    if (!validationError.isEmpty()) {
        let errors = Object.values(validationError.mapped());
        // console.log(errors);
        errorArr = errors.map((item) => {
            return item.msg;
        });
        // console.log(errorArr);
        return res.status(500).send(errorArr);
    }
    try {
        let currentUserId = req.user._id;
        let keyword = req.params.keyword;
        // console.log(currentUserId, keyword);
        let users = await findUserContactService(currentUserId, keyword);
        return res.render("main/contacts/sections/_findUsersContact", { users });

    } catch (error) {
        return res.status(500).send(error)
    }
};
const addNewContact = async(req, res) => {
    try {
        let currentUserId = req.user._id;
        let contactId = req.body.uid;

        let newContact = await addNewContactService(currentUserId, contactId);
        // console.log(newContact);
        // console.log(!!newContact);
        return res.status(200).send({ success: !!newContact });
    } catch (error) {
        return res.status(500).send(error)
    }
};
const removeContact = async(req, res) => {
    try {
        let currentUserId = req.user._id;
        let contactId = req.body.uid;

        let removeContact = await removeContactContactService(currentUserId, contactId);

        return res.status(200).send({ success: !!removeContact });
    } catch (error) {
        return res.status(500).send(error)
    }
};
const removeRequestContactSent = async(req, res) => {
    try {
        let currentUserId = req.user._id;
        let contactId = req.body.uid;

        let removeReq = await removeReqContactSentService(currentUserId, contactId);

        return res.status(200).send({ success: !!removeReq });
    } catch (error) {
        return res.status(500).send(error)
    }
};
const removeRequestContactReceived = async(req, res) => {
    try {
        let currentUserId = req.user._id;
        let contactId = req.body.uid;

        let removeReq = await removeReqContactReceivedService(currentUserId, contactId);

        return res.status(200).send({ success: !!removeReq });
    } catch (error) {
        return res.status(500).send(error)
    }
};
const approveRequestContactReceived = async(req, res) => {
    try {
        let currentUserId = req.user._id;
        let contactId = req.body.uid;

        let approveReq = await approveReqContactReceivedService(currentUserId, contactId);

        return res.status(200).send({ success: !!approveReq });
    } catch (error) {
        return res.status(500).send(error)
    }
};
const readMoreContacts = async(req, res) => {
    try {
        // get skip number from query
        let skipNumberContacts = +(req.query.skipNumber);
        // get moreItem
        let newContactUsers = await readMoreContactService(req.user._id, skipNumberContacts);
        return res.status(200).send(newContactUsers);
    } catch (error) {
        return res.status(500).send(error)
    }
};
const readMoreContactsSent = async(req, res) => {
    try {
        // get skip number from query
        let skipNumberContacts = +(req.query.skipNumber);
        // get moreItem
        let newContactUsers = await readMoreContactSentService(req.user._id, skipNumberContacts);
        return res.status(200).send(newContactUsers);
    } catch (error) {
        return res.status(500).send(error)
    }
};
const readMoreContactsReceived = async(req, res) => {
    try {
        // get skip number from query
        let skipNumberContacts = +(req.query.skipNumber);
        // get moreItem
        let newContactUsers = await readMoreContactReceivedService(req.user._id, skipNumberContacts);
        return res.status(200).send(newContactUsers);
    } catch (error) {
        return res.status(500).send(error)
    }
};
const searchFriend = async (req, res) => {
    let errorArr = [];
    const validationError = validationResult(req);
    if (!validationError.isEmpty()) {
        let errors = Object.values(validationError.mapped());
        // console.log(errors);
        errorArr = errors.map((item) => {
            return item.msg;
        });
        // console.log(errorArr);
        return res.status(500).send(errorArr);
    }
    try {
        let currentUserId = req.user._id;
        let keyword = req.params.keyword;
        // console.log(currentUserId, keyword);
        let users = await searchFriendsService(currentUserId, keyword);
        return res.render("main/groupChat/sections/_searchFriends", { users });


    } catch (error) {
        return res.status(500).send(error)
    }
};

export {
    findUserContact,
    addNewContact,
    removeRequestContactSent,
    readMoreContacts,
    readMoreContactsSent,
    readMoreContactsReceived,
    removeRequestContactReceived,
    approveRequestContactReceived,
    removeContact,
    searchFriend
}