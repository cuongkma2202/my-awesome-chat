import { Schema, model } from "mongoose";

const ChatGroupSchema = new Schema(
  {
    name: String,
    userAmount: { type: Number, min: 3, max: 9 },
    messageAmount: { type: Number, default: 0 },
    userId: String,
    menbers: [{ userId: String }],
  },
  { timestamps: true }
);
module.exports = model("chat-group", ChatGroupSchema);
