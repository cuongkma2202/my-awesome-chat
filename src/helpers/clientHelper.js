import moment from "moment";
const bufferToBase64 = (bufferFrom) =>{
  return Buffer.from(bufferFrom).toString("base64");

}
const lastItemOfArray = (array) => {
  if(!array.length){
      return [];
  }
  return array[array.length-1];

}
const convertTimestamp = (timestamp) =>{
  if(!timestamp){
    return "";
  };
  return moment(timestamp).locale("en").startOf("seconds").fromNow();
}
export {bufferToBase64, lastItemOfArray,convertTimestamp}