import mongoose from "mongoose";
const Schema = mongoose.Schema;

const ChatGroupSchema = new Schema({
    name: String,
    userAmount: { type: Number, min: 3, max: 9 },
    messageAmount: { type: Number, default: 0 },
    userId: String,
    members: [{ userId: String }],
}, { timestamps: true });
ChatGroupSchema.statics = {
    createNew(item){
        return this.create(item);
    },
    getChatGroups(userId, limit) {
        return this.find({
            "members": { $elemMatch: { "userId": userId } }
        }).sort({ "updatedAt": -1 }).limit(limit).exec();
    },
    getChatGroupsById(receiverId){
        return this.findById(receiverId).exec();
    },
    updateWhenAddNewMessages(id, newMessageAmount){
     return this.findByIdAndUpdate(id, {
         "messageAmount": newMessageAmount
     }).exec()
    },
    getChatGroupIdsByUser(userId){
        return this.find({
            "members": { $elemMatch: { "userId": userId }}
        },{_id: 1}).exec();
    },
    readMoreChatGroups(userId, skip, limit){
        return this.find({
            "members": { $elemMatch: { "userId": userId } }
        }).sort({ "updatedAt": -1 }).skip(skip).limit(limit).exec();
    },
    getChatGroupsByUserIdAndKeyword(userId, keyword, limit){
        return this.find({
            $and: [
              { "members": {$elemMatch: {"userId": userId}} },
              { "name": {"$regex": new RegExp(keyword, "i")} },
            ]
          }).sort({"updatedAt": -1}).limit(limit).exec();
    }
}

const ChatGroupModel = mongoose.model("chat-group", ChatGroupSchema);
export default ChatGroupModel;