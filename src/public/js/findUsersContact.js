function callFindUsers(element) {
    if (element.which === 13 || element.type === "click") {
        let keyword = $("#input-find-users-contact").val();
        let regex = new RegExp(/^[\s0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/);
        if (!keyword.length) {
            alertify.notify("Please insert something to search", "error", 7);
            return false;
        }
        if (!regex.test(keyword)) {
            alertify.notify("Keyword search incorrect", "error", 7);
            return false;
        }
        $.get(`/contact/find-users/${keyword}`, function(data) {
            $("#find-user ul").html(data);
            // console.log(data);
            addContact(); // js/addContact
            removeRequestContactSent(); // js/removeRequestContactSent
        });

    }
}
$(document).ready(function() {
    $("#input-find-users-contact").bind("keypress", callFindUsers);

    $("#btn-find-users-contact").bind("click", callFindUsers);
})