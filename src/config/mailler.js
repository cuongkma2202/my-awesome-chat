import nodeMailer from "nodemailer";
import { env } from "./environment.js";

const adminEmail = env.MAIL_USER;
const adminPassword = env.MAIL_PASSWORD;
const mailHost = env.MAIL_HOST;
const mailPort = env.MAIL_PORT;

let sendMail = (to, subject, httpContent) => {
    let transporter = nodeMailer.createTransport({
        host: mailHost,
        port: mailPort,
        secure: false,
        auth: {
            user: adminEmail,
            pass: adminPassword
        }
    });
    let options = {
        from: adminEmail,
        to: to,
        subject: subject,
        html: httpContent
    }
    return transporter.sendMail(options);
}

export default sendMail;