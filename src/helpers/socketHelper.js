const pushSocketIdToArray = (clients, userId, socketId) => {
  if (clients[userId]) {
    clients[userId].push(socketId);

  } else {
    clients[userId] = [socketId];
  }
  return clients
};
const emitNotifyToArray = (clients, userId, io, eventName, data) => {
  clients[userId].forEach((socketId) => {
    // io.sockets.adapter.rooms[socketId].emit("response-add-new-contact", currentUser);
    return io.to([socketId]).emit(eventName, data);
    // console.log(socketId);
  });
};
const removeSocketIdFromArray = (clients, userId, socket) => {
  clients[userId] = clients[userId].filter((socketId) => {
    return socketId !== socket.id;
  });
  if (!clients[userId].length) {
    delete clients[userId];
  }
  return clients;
};

export { pushSocketIdToArray, emitNotifyToArray, removeSocketIdFromArray }