import ContactModel from "../models/contactModel.js";
import UserModel from "../models/userModel.js";
import ChatGroupModel from "../models/chatGroupModel.js";
import Message from "../models/messageModel.js";
import _ from "lodash";
import { transError } from "../../lang/vi.js";
import app from "../config/app.js";
import fsExtra from "fs-extra";

const LIMIT_CONVERSATIONS = 15;
const LIMIT_MESSAGES = 30;
/**
 * get All Item chat
 * @param {String} currentUserId
 */
const getAllConversationItemService = (currentUserId) => {
  return new Promise(async (resolve, reject) => {
    try {
      let contacts = await ContactModel.getContacts(
        currentUserId,
        LIMIT_CONVERSATIONS
      );

      let userConversationsPromise = contacts.map(async (contact) => {
        // console.log(contact);
        if (contact.contactId == currentUserId) {
          let getUserContact = await UserModel.getNormalUserDataById(
            contact.userId
          );

          getUserContact = getUserContact.toObject();

          getUserContact.updatedAt = contact.updatedAt;
          return getUserContact;
        } else {
          let getUserContact = await UserModel.getNormalUserDataById(
            contact.contactId
          );

          getUserContact = getUserContact.toObject();
          getUserContact.updatedAt = contact.updatedAt;
          return getUserContact;
        }
      });
      let userConversations = await Promise.all(userConversationsPromise);

      let groupConversations = await ChatGroupModel.getChatGroups(
        currentUserId,
        LIMIT_CONVERSATIONS
      );
      groupConversations = groupConversations.map((item) => {
        return item.toObject();
      });
      let allConversations = userConversations.concat(groupConversations);
      allConversations = _.sortBy(allConversations, (item) => {
        return -item.updatedAt;
      });
      // get message
      let allConversationsWithMessagesPromise = allConversations.map(
        async (conversation) => {
          if (conversation.members) {
            let getMessages = await Message.MessageModel.getMessagesInGroup(
              conversation._id,
              LIMIT_MESSAGES
            );
            conversation["messages"] = _.reverse(getMessages);
            conversation.membersInfo = [];
            for(let member of conversation.members){
              let userInfo = await UserModel.getNormalUserDataById(member.userId);
              conversation.membersInfo.push(userInfo);
            }

          } else {
            let getMessages = await Message.MessageModel.getMessagesInPersonal(
              currentUserId,
              conversation._id,
              LIMIT_MESSAGES
            );
            conversation["messages"] = _.reverse(getMessages);


          }
          return conversation;
        }
      );
      let allConversationsWithMessages = await Promise.all(
        allConversationsWithMessagesPromise
      );
      // sort by updated At message des
      allConversationsWithMessages = _.sortBy(
        allConversationsWithMessages,
        (item) => {
          return -item.updatedAt;
        }
      );

      resolve({
        allConversationsWithMessages: allConversationsWithMessages,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const searchConversationService = (currentUserId, keyword) => {
  return new Promise(async (resolve, reject) => {
    try {
      let contacts = await ContactModel.getContacts(
        currentUserId,
        LIMIT_CONVERSATIONS
      );

      let userConversationsPromise = contacts.map(async (contact) => {
        // console.log(contact);
        if (contact.contactId == currentUserId) {
          let getUserContact = await UserModel.fillAllToAddGroupChat(
            contact.userId,
            keyword
          );
          if(getUserContact.length){
            getUserContact.updatedAt = contact.updatedAt;
            return getUserContact;
          }
        } else {
          let getUserContact = await UserModel.fillAllToAddGroupChat(
            contact.contactId,
            keyword
          );
          if(getUserContact.length){
            getUserContact.updatedAt = contact.updatedAt;
            return getUserContact;
          }
        }
      });
      let userConversations = await Promise.all(userConversationsPromise);
      userConversations = _.filter(userConversations, user => {
        return typeof user !== "undefined";
      });

      let groupConversations = await ChatGroupModel.getChatGroupsByUserIdAndKeyword(
        currentUserId,
        keyword,
        LIMIT_CONVERSATIONS
      );
      groupConversations = groupConversations.map((item) => {
        return item.toObject();
      });
      let allConversations = userConversations.concat(groupConversations);
      allConversations = _.sortBy(allConversations, (item) => {
        return -item.updatedAt;
      });

      resolve(allConversations);
    } catch (error) {
      reject(error);
    }
  });
};
/**
 * add new message text
 * @param {Object} sender
 * @param {String} receiverId // id personal or group chat
 * @param {String} messageVal
 * @param {boolean} isChatGroup
 */
const addNewTextEmojiService = (
  sender,
  receiverId,
  messageVal,
  isChatGroup
) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (isChatGroup) {
        let chatGroupReceiver = await ChatGroupModel.getChatGroupsById(
          receiverId
        );
        if (!chatGroupReceiver) {
          return reject(transError.conversation_not_found);
        }
        let receiver = {
          id: chatGroupReceiver._id,
          name: chatGroupReceiver.name,
          avatar: app.general_avatar_group_chat,
        };
        let newMessageItem = {
          senderId: sender.id,
          receiverId: receiver.id,
          conversationType: Message.MESSAGE_CONVERSATION_TYPE.GROUP,
          messageType: Message.MESSAGE_TYPE.TEXT,
          sender: sender,
          receiver: receiver,
          text: messageVal,
        };
        let newMessage = await Message.MessageModel.createNew(newMessageItem);
        await ChatGroupModel.updateWhenAddNewMessages(chatGroupReceiver._id, chatGroupReceiver.messageAmount + 1)
        resolve(newMessage);
      } else {
        let chatUserReceiver = await UserModel.getNormalUserDataById(
          receiverId
        );
        if (!chatUserReceiver) {
          return reject(transError.conversation_not_found);
        }
        let receiver = {
          id: chatUserReceiver._id,
          name: chatUserReceiver.username,
          avatar: chatUserReceiver.avatar,
        };
        let newMessageItem = {
          senderId: sender.id,
          receiverId: receiver.id,
          conversationType: Message.MESSAGE_CONVERSATION_TYPE.PERSONAL,
          messageType: Message.MESSAGE_TYPE.TEXT,
          sender: sender,
          receiver: receiver,
          text: messageVal,
        };
        let newMessage = await Message.MessageModel.createNew(newMessageItem);
        // updateContact
        await ContactModel.updateWhenHasNewMessage(sender.id, chatUserReceiver._id);
        resolve(newMessage);
      }
    } catch (error) {
      reject(error);
    }
  });
};
const addNewImageMessageService = (
  sender,
  receiverId,
  messageVal,
  isChatGroup
) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (isChatGroup) {
        let chatGroupReceiver = await ChatGroupModel.getChatGroupsById(
          receiverId
        );
        if (!chatGroupReceiver) {
          return reject(transError.conversation_not_found);
        }
        let receiver = {
          id: chatGroupReceiver._id,
          name: chatGroupReceiver.name,
          avatar: app.general_avatar_group_chat,
        };
        let imageBuffer = await fsExtra.readFile(messageVal.path);
      let imageContentType = messageVal.minetype;
      let imageName = messageVal.originalname;
        let newMessageItem = {
          senderId: sender.id,
          receiverId: receiver.id,
          conversationType: Message.MESSAGE_CONVERSATION_TYPE.GROUP,
          messageType: Message.MESSAGE_TYPE.IMAGE,
          sender: sender,
          receiver: receiver,
          file: {
            data: imageBuffer,
            contentType: imageContentType,
            fileName: imageName
          }
        };
        let newMessage = await Message.MessageModel.createNew(newMessageItem);
        await ChatGroupModel.updateWhenAddNewMessages(chatGroupReceiver._id, chatGroupReceiver.messageAmount + 1)
        resolve(newMessage);
      } else {
        let chatUserReceiver = await UserModel.getNormalUserDataById(
          receiverId
        );
        if (!chatUserReceiver) {
          return reject(transError.conversation_not_found);
        }
        let receiver = {
          id: chatUserReceiver._id,
          name: chatUserReceiver.username,
          avatar: chatUserReceiver.avatar,
        };
        let imageBuffer = await fsExtra.readFile(messageVal.path);
        let imageContentType = messageVal.minetype;
        let imageName = messageVal.originalname;
        let newMessageItem = {
          senderId: sender.id,
          receiverId: receiver.id,
          conversationType: Message.MESSAGE_CONVERSATION_TYPE.PERSONAL,
          messageType: Message.MESSAGE_TYPE.IMAGE,
          sender: sender,
          receiver: receiver,
          file: {
            data: imageBuffer,
            contentType: imageContentType,
            fileName: imageName
          }
        };
        let newMessage = await Message.MessageModel.createNew(newMessageItem);
        // updateContact
        await ContactModel.updateWhenHasNewMessage(sender.id, chatUserReceiver._id);
        resolve(newMessage);
      }
    } catch (error) {
      reject(error);
    }
  });
};
const addNewAttachmentMessageService = (
  sender,
  receiverId,
  messageVal,
  isChatGroup
) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (isChatGroup) {
        let chatGroupReceiver = await ChatGroupModel.getChatGroupsById(
          receiverId
        );
        if (!chatGroupReceiver) {
          return reject(transError.conversation_not_found);
        }
        let receiver = {
          id: chatGroupReceiver._id,
          name: chatGroupReceiver.name,
          avatar: app.general_avatar_group_chat,
        };
        let attachmentBuffer = await fsExtra.readFile(messageVal.path);
        let attachmentContentType = messageVal.minetype;
        let attachmentName = messageVal.originalname;
        let newMessageItem = {
          senderId: sender.id,
          receiverId: receiver.id,
          conversationType: Message.MESSAGE_CONVERSATION_TYPE.GROUP,
          messageType: Message.MESSAGE_TYPE.FILE,
          sender: sender,
          receiver: receiver,
          file: {
            data: attachmentBuffer,
            contentType: attachmentContentType,
            fileName: attachmentName
          }
        };
        let newMessage = await Message.MessageModel.createNew(newMessageItem);
        await ChatGroupModel.updateWhenAddNewMessages(chatGroupReceiver._id, chatGroupReceiver.messageAmount + 1)
        resolve(newMessage);
      } else {
        let chatUserReceiver = await UserModel.getNormalUserDataById(
          receiverId
        );
        if (!chatUserReceiver) {
          return reject(transError.conversation_not_found);
        }
        let receiver = {
          id: chatUserReceiver._id,
          name: chatUserReceiver.username,
          avatar: chatUserReceiver.avatar,
        };
        let attachmentBuffer = await fsExtra.readFile(messageVal.path);
        let attachmentContentType = messageVal.minetype;
        let attachmentName = messageVal.originalname;
        let newMessageItem = {
          senderId: sender.id,
          receiverId: receiver.id,
          conversationType: Message.MESSAGE_CONVERSATION_TYPE.PERSONAL,
          messageType: Message.MESSAGE_TYPE.FILE,
          sender: sender,
          receiver: receiver,
          file: {
            data: attachmentBuffer,
            contentType: attachmentContentType,
            fileName: attachmentName
          }
        };
        let newMessage = await Message.MessageModel.createNew(newMessageItem);
        // updateContact
        await ContactModel.updateWhenHasNewMessage(sender.id, chatUserReceiver._id);
        resolve(newMessage);
      }
    } catch (error) {
      reject(error);
    }
  });
};

const readMoreAllChatService = (currentUserId, skipPersonalNumber, skipGroupNumber) =>{
  return new Promise(async (resolve, reject) => {
    try {
      let contacts = await ContactModel.readMoreContacts(
        currentUserId,
        skipPersonalNumber,
        LIMIT_CONVERSATIONS
      );
      let userConversationsPromise = contacts.map(async (contact) => {
        // console.log(contact);
        if (contact.contactId == currentUserId) {
          let getUserContact = await UserModel.getNormalUserDataById(
            contact.userId
          );

          getUserContact = getUserContact.toObject();

          getUserContact.updatedAt = contact.updatedAt;
          return getUserContact;
        } else {
          let getUserContact = await UserModel.getNormalUserDataById(
            contact.contactId
          );

          getUserContact = getUserContact.toObject();
          getUserContact.updatedAt = contact.updatedAt;
          return getUserContact;
        }
      });
      let userConversations = await Promise.all(userConversationsPromise);

      let groupConversations = await ChatGroupModel.readMoreChatGroups(
        currentUserId,
        skipGroupNumber,
        LIMIT_CONVERSATIONS
      );


      groupConversations = groupConversations.map((item) => {
        return item.toObject();
      });
      let allConversations = userConversations.concat(groupConversations);
      allConversations = _.sortBy(allConversations, (item) => {
        return -item.updatedAt;
      });
      // get message
      let allConversationsWithMessagesPromise = allConversations.map(
        async (conversation) => {
          if (conversation.members) {
            let getMessages = await Message.MessageModel.getMessagesInGroup(
              conversation._id,
              LIMIT_MESSAGES
            );
            conversation.membersInfo = [];
            for(let member of conversation.members){
              let userInfo = await UserModel.getNormalUserDataById(member.userId);
              conversation.membersInfo.push(userInfo);
            }
            conversation["messages"] = _.reverse(getMessages);

          } else {
            let getMessages = await Message.MessageModel.getMessagesInPersonal(
              currentUserId,
              conversation._id,
              LIMIT_MESSAGES
            );
            conversation["messages"] = _.reverse(getMessages);

          }
          return conversation;
        }
      );
      let allConversationsWithMessages = await Promise.all(
        allConversationsWithMessagesPromise
      );
      // sort by updated At message des
      allConversationsWithMessages = _.sortBy(
        allConversationsWithMessages,
        (item) => {
          return -item.updatedAt;
        }
      );

      resolve(allConversationsWithMessages);
    } catch (error) {
      reject(error);
    }
  });
};
const readMoreMessageService = (currentUserId, skipMessage, targetId, chatInGroup) =>{
  return new Promise(async (resolve, reject) => {
    try {
      // message in group
      if (chatInGroup) {
        let getMessages = await Message.MessageModel.readMoreMessagesInGroup(
          targetId,
          skipMessage,
          LIMIT_MESSAGES
        );
        getMessages = _.reverse(getMessages);
        return resolve(getMessages);

      }
      // message personal
        let getMessages = await Message.MessageModel.readMoreMessagesInPersonal(
          currentUserId,
          targetId,
          skipMessage,
          LIMIT_MESSAGES
        );
        getMessages = _.reverse(getMessages);
        return resolve(getMessages);
    } catch (error) {
      reject(error);
    }
  });


};
const readMorePersonalChatService = (currentUserId, skipPersonalNumber) =>{
  return new Promise(async (resolve, reject) => {
    try {
      let contacts = await ContactModel.readMoreContacts(
        currentUserId,
        skipPersonalNumber,
        LIMIT_CONVERSATIONS
      );
      let userConversationsPromise = contacts.map(async (contact) => {
        // console.log(contact);
        if (contact.contactId == currentUserId) {
          let getUserContact = await UserModel.getNormalUserDataById(
            contact.userId
          );

          getUserContact = getUserContact.toObject();
          getUserContact.updatedAt = contact.updatedAt;
          return getUserContact;
        }
      });
      let userConversations = await Promise.all(userConversationsPromise);
      userConversations = _.sortBy(userConversations, (item) => {
        return -item.updatedAt;
      });
      // get message
      let userConversationsWithMessagesPromise = userConversations.map(
        async (conversation) => {

            let getMessages = await Message.MessageModel.getMessagesInPersonal(
              currentUserId,
              conversation._id,
              LIMIT_MESSAGES
            );
            conversation["messages"] = _.reverse(getMessages);


          return conversation;
        }
      );
      let userConversationsWithMessages = await Promise.all(
        userConversationsWithMessagesPromise
      );
      // sort by updated At message des
      userConversationsWithMessages = _.sortBy(
        userConversationsWithMessages,
        (item) => {
          return -item.updatedAt;
        }
      );

      resolve(userConversationsWithMessages);
    } catch (error) {
      reject(error);
    }
  });
};
const readMoreGroupChatService = (currentUserId, skipGroupNumber) =>{
  return new Promise(async (resolve, reject) => {
    try {
      let contacts = await ContactModel.readMoreContacts(
        currentUserId,
        LIMIT_CONVERSATIONS
      );
      let groupConversations = await ChatGroupModel.readMoreChatGroups(
        currentUserId,
        skipGroupNumber,
        LIMIT_CONVERSATIONS
      );


      groupConversations = groupConversations.map((item) => {
        return item.toObject();
      });
      // get message
      let groupConversationWithMessagesPromise = groupConversations.map(
        async (conversation) => {
            let getMessages = await Message.MessageModel.getMessagesInGroup(
              conversation._id,
              LIMIT_MESSAGES
            );
            conversation["messages"] = _.reverse(getMessages);

            conversation.membersInfo = [];
            for(let member of conversation.members){
              let userInfo = await UserModel.getNormalUserDataById(member.userId);
              conversation.membersInfo.push(userInfo);
            }
          return conversation;
        }
      );
      let groupConversationWithMessages = await Promise.all(
        groupConversationWithMessagesPromise
      );
      // sort by updated At message des
      groupConversationWithMessages = _.sortBy(
        groupConversationWithMessages,
        (item) => {
          return -item.updatedAt;
        }
      );

      resolve(groupConversationWithMessages);
    } catch (error) {
      reject(error);
    }
  });
};

export {
  getAllConversationItemService,
  addNewTextEmojiService,
  addNewImageMessageService,
  addNewAttachmentMessageService,
  readMoreAllChatService,
  readMoreMessageService,
  searchConversationService,
  readMorePersonalChatService,
  readMoreGroupChatService
};
