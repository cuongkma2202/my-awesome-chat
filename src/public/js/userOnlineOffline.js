// step special
socket.emit("check-status");

// step 1: listen
socket.on("server-send-list-user-online", function(listUserIds){

listUserIds.forEach(userId => {
$(`.person[data-chat =${userId}]`).find("div.dot").addClass("online");
$(`.person[data-chat =${userId}]`).find("img").addClass("avatar-online");

});
});
// step2: new user online
socket.on("server-send-when-new-user-online", function(userId){
  $(`.person[data-chat =${userId}]`).find("div.dot").addClass("online");
  $(`.person[data-chat =${userId}]`).find("img").addClass("avatar-online");
});
// step3 user offime
socket.on("server-send-when-new-user-offline", function(userId){
  $(`.person[data-chat =${userId}]`).find("div.dot").removeClass("online");
  $(`.person[data-chat =${userId}]`).find("img").removeClass("avatar-online");
});