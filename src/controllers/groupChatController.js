import { validationResult } from "express-validator";
import { addNewGroupService } from "../services/groupChatService.js";
const addNewGroup = async (req, res) => {
  let errorArr = [];
  const validationError = validationResult(req);
  if (!validationError.isEmpty()) {
      let errors = Object.values(validationError.mapped());
      // console.log(errors);
      errorArr = errors.map((item) => {
          return item.msg;
      });
      // console.log(errorArr);
      return res.status(500).send(errorArr);
  }
  try {
        let currentUserId = req.user._id;
        let arrayMemberIds = req.body.arrayIds;
        let groupChatName = req.body.groupChatName;
        let newGroupChat = await addNewGroupService(currentUserId, arrayMemberIds, groupChatName);
        return res.status(200).send({groupChat: newGroupChat});
  } catch (error) {
      return res.status(500).send(error)
  }
};
export {addNewGroup}