import express from "express";
import connectToDb from "./config/connectDb.js";
import dotenv from "dotenv";
import configViewEngine from "./config/viewEngine.js";
import initRoute from "./routes/web.js";
import bodyParser from "body-parser";
import connectFlash from "connect-flash";
import { configSession, sessionStore } from "./config/session.js";
import passport from "passport";
import http from "http";
import { Server } from "socket.io";
import configSocketIo from "./config/socketio.js";
import events from "events";


import cookieParser from "cookie-parser";

// import pem from "pem";
// import https from "https";
import initSockets from './sockets/index.js';

dotenv.config();

// pem.createCertificate({ days: 1, selfSigned: true }, function(err, keys) {
//     if (err) {
//         throw err;
//     }
//     // init app
//     const app = express();
//     // Connect to DB
//     connectToDb();
//     // config session
//     configSession(app);
//     // connect to view Engine
//     configViewEngine(app);
//     // enable body-parser
//     app.use(bodyParser.urlencoded({ extended: true }));
//     app.use(express.json());
//     app.use(express.urlencoded({ extended: true }));
//     // enable flash
//     app.use(connectFlash());
//     // config passport
//     app.use(passport.initialize());
//     app.use(passport.session());
//     // routes
//     initRoute(app);
//     const PORT = process.env.PORT || 3000;
//     https
//         .createServer({ key: keys.clientKey, cert: keys.certificate }, app)
//         .listen(PORT, () => {
//             console.log(`App running on port ${PORT}`);
//         });
// });

// init app
const app = express();
events.EventEmitter.defaultMaxListeners = +process.env.MAX_EVENT_LISTENER;
// init server socketIP + express
const server = http.createServer(app);
const io = new Server(server);

// Connect to DB
connectToDb();
// config session
configSession(app);
// connect to view Engine
configViewEngine(app);
// enable body-parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// enable flash
app.use(connectFlash());
// use cookie-parser
app.use(cookieParser());
// config passport
app.use(passport.initialize());
app.use(passport.session());
// routes
initRoute(app);

// config socket.io
configSocketIo(io, cookieParser, sessionStore);
// init all socket
initSockets(io);
const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
    console.log(`App running on port ${PORT}`);
});