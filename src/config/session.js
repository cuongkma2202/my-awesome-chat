import session from "express-session";
import MongoStore from "connect-mongo";
import { env } from "../config/environment.js"

let sessionStore = MongoStore.create({
    mongoUrl: `mongodb://localhost:27017/awesome_chat`,
    autoReconnect: true,
    autoRemove: "native"
})

const configSession = (app) => {
    app.use(session({
        key: env.SESSION_KEY,
        secret: env.SESSION_SECRET,
        store: sessionStore,
        resave: true,
        saveUninitialized: true,
        cookie: {
            maxAge: 1000 * 60 * 60 * 24
        }
    }))
    console.log(`${process.env.DB_CONNECTION}://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`);
}
export { configSession, sessionStore }