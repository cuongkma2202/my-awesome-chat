import passport from "passport";
import passportFacebook from "passport-facebook";
import User from "../../models/userModel.js";
import { transError, transSuccess } from "../../../lang/vi.js";
import { env } from "../../config/environment.js";
import ChatGroupModel from "../../models/chatGroupModel.js"

const FacebookStrategy = passportFacebook.Strategy;
/**
 * Valid user account type facebook
 */
let initPassportFacebook = () => {
    passport.use(
        new FacebookStrategy({
                clientID: env.FACEBOOK_APP_ID,
                clientSecret: env.FACEBOOK_APP_SECRET,
                callbackURL: env.FACEBOOK_CALLBACK_URL,
                profileFields: ["email", "gender", "displayName"],
                passReqToCallback: true,
            },
            async(req, accessToken, refreshToken, profile, done) => {
                try {
                    const user = await User.findByFacebookUid(profile.id);
                    if (user) {
                        // console.log(user);
                        return done(
                            null,
                            user,
                            req.flash("success", transSuccess.loginSuccess(user.username))
                        );
                    }
                    // console.log(profile);
                    let newUserItem = {
                        username: profile.displayName,
                        gender: profile.gender,
                        local: {
                            isActive: true,
                        },
                        facebook: {
                            uid: profile.id,
                            token: accessToken,
                            email: profile.emails[0].value,
                        },
                    };
                    const newUser = await User.createNew(newUserItem);
                    return done(
                        null,
                        newUser,
                        req.flash("success", transSuccess.loginSuccess(newUser.username))
                    );
                } catch (error) {
                    // console.log(error);
                    return done(
                        null,
                        false,
                        req.flash("errors", transError.server_error)
                    );
                }
            }
        )
    );
    // Save userID to Session
    passport.serializeUser((user, done) => {
        done(null, user._id);
    });
    passport.deserializeUser(async(id, done) => {
        try {
            let user = await User.findUserByIdForSessionToUse(id);
            let getChatGroupIds = await ChatGroupModel.getChatGroupIdsByUser(user._id);

            user = user.toObject();
            user.chatGroupIds = getChatGroupIds;

            return done(null, user);
          } catch (error) {
            return done(error, null);
          }
    });
};
export { initPassportFacebook };