
function imageChat(divId){
$(`#image-chat-${divId}`).unbind("change").on("change", function(){
  const fileData = $(this).prop("files")[0];
  const math = ["image/png", "image/jpg", "image/jpeg"];
  const limit = 1048576; // byte = 1mb

  if ($.inArray(fileData.type, math) === -1) {
    alertify.notify("File does not support. JPG, PNG only.", "error", 7);
    $(this).val(null);
    return false;
  };

  if (fileData.size > limit) {
    alertify.notify("Upload image no larger than 1MB", "error", 7);
    $(this).val(null);
    return false;
  };

  let targetId = $(this).data("chat");
  let isChatGroup = false;

  let messageFormData = new FormData();

  messageFormData.append("my-image-chat", fileData);
  messageFormData.append("uid", targetId);

  if ($(this).hasClass("chat-in-group")) {
    messageFormData.append("isChatGroup", true);
    isChatGroup = true;
  }
  $.ajax({
    url: "/message/add-new-image",
    method: "post",
    cache: false,
    contentType: false,
    processData: false,
    data: messageFormData,
    success: function (data) {;
      let dataToEmit = {
        message: data.message,
      };
      let divContent = $(`<div class="chat-content me" data-mess-id="${data.message._id}"></div>`)
      let messageOfMe = $(`
          <div class="convert-emoji bubble me bubble-image-file" data-mess-id="${data.message._id}"></div>
          `);
          let imageChat = `<img src="data:${data.message.file.contentType}; base64, ${bufferToBase64(data.message.file.data.data)}" class="show-image-chat" />`

            if (isChatGroup) {
              let userInfo = `<div class="infor-user-chat me">
              <p>${data.message.sender.name}</p>
              <img src="/images/users/${data.message.sender.avatar}" class="avatar-small" title="${data.message.sender.name}"/>
             </div>`;
              divContent.html(`${userInfo}`).append( messageOfMe.html(`${imageChat}`));
              increaseNumberMessageGroup(divId);
              dataToEmit.groupId = targetId;
            } else {
              messageOfMe.html(imageChat);
              dataToEmit.contactId = targetId;
            }

            // 2. append message to screen

            if(isChatGroup){
              $(`.right .chat[data-chat=${divId}]`).append(divContent);
              nineScrollRight(divId);
            }else{
              $(`.right .chat[data-chat=${divId}]`).append(messageOfMe);
              nineScrollRight(divId)
            }
            // 3. remove input: nothing .
            // 4. change preview and timestamp in leftSide
            $(`.person[data-chat = ${divId}]`)
              .find("span.time").removeClass("message-realtime")
              .html(
                moment(data.message.createdAt)
                  .locale("en")
                  .startOf("seconds")
                  .fromNow()
              );
            $(`.person[data-chat = ${divId}]`)
              .find("span.preview")
              .html("Image....");

            // 5. move conversation to top
            $(`.person[data-chat = ${divId}]`).click(
              "triggerEvent.moveConversationToTop",
              function () {
                let dataToMove = $(this).parent();
                $(this).closest("ul").prepend(dataToMove);
                $(this).off("triggerEvent.moveConversationToTop");
              }
            );
            $(`.person[data-chat = ${divId}]`).click();

            // 6. emit realtime
            socket.emit("chat-image", dataToEmit);
            // 7.emit remove typing realtime: nothing
            // 8.if this has typing remove remove gif: nothing
            // 9.add new image to model image
            let imageChatToAddModal = `<img  src="data:${data.message.file.contentType}; base64, ${bufferToBase64(data.message.file.data.data)}"/>`
            $(`#imagesModal_${divId}`).find(".all-images").append(imageChatToAddModal);
            zoomImageChat();

    },
    error: function (error) {
      alertify.notify(error.responseText, "error", 7);
    }
  })
});
}
$(document).ready(function(){
socket.on("response-chat-image", function(response){
  let divId = "";
  let divContent = $(`<div class="chat-content you" data-mess-id="${response.message._id}"></div>`);
  messageOfYou = $(`
  <div class="bubble you bubble-image-file"  data-mess-id="${response.message._id}"></div>
  `);
  messageOfYou.text(response.message.text);
    let imageChat = `<img src="data:${response.message.file.contentType}; base64, ${bufferToBase64(response.message.file.data.data)}" class="show-image-chat" />`
    if (response.currentGroupId) {
      divId = response.currentGroupId;
      let userInfo = `<div class="infor-user-chat you">

              <img src="/images/users/${response.message.sender.avatar}" class="avatar-small" title="${response.message.sender.name}"/>
              <p>${response.message.sender.name}</p>
             </div>`;
              divContent.html(`${userInfo}`).append(messageOfYou.html(`${imageChat}`));
      if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
        increaseNumberMessageGroup(divId);
      }
    } else {
      divId = response.currentUserId;
      messageOfYou.html(imageChat);
    }

    // 2. append message to screen
    if(response.currentGroupId){
      if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
        $(`.right .chat[data-chat=${divId}]`).append(divContent);
        nineScrollRight(divId);
        $(`.person[data-chat = ${divId}]`)
        .find("span.time")
        .addClass("message-realtime")
      }
    }else{
      if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
        $(`.right .chat[data-chat=${divId}]`).append(messageOfYou);
        nineScrollRight(divId);
        $(`.person[data-chat = ${divId}]`)
        .find("span.time")
        .addClass("message-realtime")
      }
    }

    // 3. remove input :nothing
    // 4. change preview and timestamp in leftSide
    $(`.person[data-chat = ${divId}]`)
      .find("span.time")
      .html(
        moment(response.message.createdAt)
          .locale("en")
          .startOf("seconds")
          .fromNow()
      );
    $(`.person[data-chat = ${divId}]`)
      .find("span.preview")
      .html("Image....");


      // 5. move conversation to top
    $(`.person[data-chat = ${divId}]`).click(
      "triggerEvent.moveConversationToTop",
      function () {
        let dataToMove = $(this).parent();
        $(this).closest("ul").prepend(dataToMove);
        $(this).off("triggerEvent.moveConversationToTop");
      }
    );
    $(`.person[data-chat = ${divId}]`).trigger("triggerEvent.moveConversationToTop");

     // 9.add new image to model image
     if(response.currentUserId !== $("#dropdown-navbar-user").data("uid")){
      let imageChatToAddModal = `<img  src="data:${response.message.file.contentType}; base64, ${bufferToBase64(response.message.file.data.data)}"/>`
      $(`#imagesModal_${divId}`).find(".all-images").append(imageChatToAddModal);
     }


     zoomImageChat();
});
});