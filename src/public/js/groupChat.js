function addFriendsToGroup() {
  $("ul#group-chat-friends")
    .find("div.add-user")
    .bind("click", function () {
      let uid = $(this).data("uid");
      $(this).remove();
      let html = $("ul#group-chat-friends").find(`div[data-uid=${uid}]`).html();

      let promise = new Promise(function (resolve, reject) {
        $("ul#friends-added").append(html);
        $("#groupChatModal .list-user-added").show();
        resolve(true);
      });
      promise.then(function (success) {
        $("ul#group-chat-friends")
          .find("div[data-uid=" + uid + "]")
          .remove();
      });
    });
}

function cancelCreateGroup() {
  $("#btn-cancel-group-chat").bind("click", function () {
    $("#groupChatModal .list-user-added").hide();
    if ($("ul#friends-added>li").length) {
      $("ul#friends-added>li").each(function (index) {
        $(this).remove();
      });
    }
  });
}
function callSearchFriends(element) {
  if (element.which === 13 || element.type === "click") {
    let keyword = $("#input-search-friends-to-add-group-chat").val();
    let regex = new RegExp(
      /^[\s0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/
    );
    if (!keyword.length) {
      alertify.notify("Please insert something to search", "error", 7);
      return false;
    }
    if (!regex.test(keyword)) {
      alertify.notify("Keyword search incorrect", "error", 7);
      return false;
    }
    $.get(`/contact/search-friends/${keyword}`, function (data) {
      $("ul#group-chat-friends").html(data);
      // console.log(data);

      addFriendsToGroup();

      cancelCreateGroup();
    });
  }
}
function callCreateGroupChat() {
  $("#btn-create-group-chat")
    .unbind("click")
    .on("click", function () {
      let countUsers = $("ul#friends-added").find("li");
      if (countUsers.length < 2) {
        alertify.notify("Minium 3 member", "error", 5);
        return false;
      }
      let groupChatName = $("#input-name-group-chat").val();
      let regexGroupChatName = new RegExp(
        /^[\s0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/
      );
      if (
        !regexGroupChatName.test(groupChatName) ||
        groupChatName.length < 5 ||
        groupChatName.length > 30
      ) {
        alertify.notify("Invalid group chat name !", "error", 5);
        return false;
      }
      let arrayIds = [];
      $("ul#friends-added")
        .find("li")
        .each(function (index, item) {
          arrayIds.push({ userId: $(item).data("uid") });
        });
      Swal.fire({
        title: `Are you sure create &nbsp; ${groupChatName} group chat?`,
        text: "You won't be able to revert this!",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, Create it!",
      }).then((result) => {
        if (!result.value) {
          return false;
        }
        // post data to server
        $.post(
          "/group-chat/add-new",
          {
            arrayIds: arrayIds,
            groupChatName: groupChatName,
          },
          function (data) {
            // 1. hide modal
            $("#input-name-group-chat").val("");
            $("#btn-cancel-group-chat").click();
            $("#groupChatModal").modal("hide");

            // 2. handle left-side data
            let subGroupChatName = data.groupChat.name;
            if (subGroupChatName.length > 15) {
              subGroupChatName = subGroupChatName.substr(0, 13);
            }
            let leftSideData = `
    <a href="#uid_${data.groupChat._id}" class="room-chat" data-target="#to_${data.groupChat._id}">
                                <li class="person group-chat" data-chat="${data.groupChat._id}">
                                    <div class="left-avatar">
                                        <img src="images/users/group-avatar-trungquandev.png" alt="" />
                                    </div>
                                    <p class="name">
                                        <span class="group-chat-name">
                                            ${subGroupChatName}
                                        </span>
                                    </p>
                                    <span class="time">
                                   </span>
                                    <span class="preview convert-emoji">
                                    </span>
                                </li>
                            </a>
    `;
            $("#all-chat").find("ul").prepend(leftSideData);
            $("#group-chat").find("ul").prepend(leftSideData);
            // 3. handle right side
            let rightSideData = `
    <div class="right tab-pane" data-chat="${data.groupChat._id}" id="to_${data.groupChat._id}">
    <div class="top">
        <span>To: <span class="name">${data.groupChat.name}</span></span>
        <span class="chat-menu-right">
          <a href="#attachmentsModal_${data.groupChat._id}" class="show-attachments" data-toggle="modal">
          Attachment
            <i class="fa fa-paperclip"></i>
          </a>
        </span>
        <span class="chat-menu-right">
          <a href="javascript:void(0)">&nbsp;</a>
        </span>
        <span class="chat-menu-right">
          <a href="#imagesModal_${data.groupChat._id}" class="show-images" data-toggle="modal">
            Image
            <i class="fa fa-photo"></i>
          </a>
        </span>
        <span class="chat-menu-right">
          <a href="javascript:void(0)">&nbsp;</a>
        </span>
        <span class="chat-menu-right">
          <a href="#membersModal_${response.groupChat._id}" class="number-members" data-toggle="modal">
            <span class="show-number-members">${data.groupChat.userAmount}</span>

            <i class="fa fa-users"></i>
          </a>
        </span>
        <span class="chat-menu-right">
          <a href="javascript:void(0)">&nbsp;</a>
        </span>
        <span class="chat-menu-right">
          <a href="javascript:void(0)" class="number-messages" data-toggle="modal">
            <span class="show-number-messages">${data.groupChat.messageAmount}</span>

            <i class="fa fa-comment"></i>
          </a>
        </span>
      </div>
      <div class="content-chat">
        <div class="chat chat-in-group" data-chat="${data.groupChat._id}">
        </div>
      </div>
      <div class="write" data-chat="${data.groupChat._id}">
        <input type="text" id="write-chat-${data.groupChat._id}" class="write-chat chat-in-group" data-chat="${data.groupChat._id}" />
        <div class="icons">
          <a href="#" class="icon-chat" data-chat="${data.groupChat._id}"
            ><i class="fa fa-smile-o"></i
          ></a>
          <label for="image-chat-${data.groupChat._id}" class="image-chat-label">
            <input
              type="file"
              id="image-chat-${data.groupChat._id}"
              name="my-image-chat"
              class="image-chat chat-in-group"
              data-chat="${data.groupChat._id}"
            />
            <i class="fa fa-photo"></i>
          </label>
          <label for="attachment-chat-${data.groupChat._id}" class="attachment-chat-label">
            <input
              type="file"
              id="attachment-chat-${data.groupChat._id}"
              name="my-attachment-chat"
              class="attachment-chat chat-in-group"
              data-chat="${data.groupChat._id}"
            />
            <i class="fa fa-paperclip"></i>
          </label>
          <a
            href="#streamModal"
            id="video-chat-group"
          >
            <i class="fa fa-video-camera"></i>
          </a>
        </div>
      </div>
</div>
    `;
            $("#screen-chat").prepend(rightSideData);
            // 4. call change screen chat - js/mainConfig.js
            changeScreenChat();
            // 5. handle image modal
            let imageModalData = `
                <div class="modal fade" id="imagesModal_${data.groupChat._id}" role="dialog">
                    <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                      &times;
                    </button>
                    <h4 class="modal-title">All Images in this conversation</h4>
                  </div>
                  <div class="modal-body">
                    <div class="all-images" style="visibility: hidden;">
                    </div>
                  </div>
                </div>
              </div>
            </div>
    `;
            $("body").append(imageModalData);
            // 6. call  gridPhoto = js/mainConfig.js
            gridPhotos(5);
            // 7.handle attachment modal
            let attachmentModalData = `
          <div class="modal fade" id="attachmentsModal_${data.groupChat._id}" role="dialog">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">
             &times;
           </button>
           <h4 class="modal-title">All Attachments in this conversation.</h4>
             </div>
               <div class="modal-body">
                   <ul class="list-attachments">
                   </ul>
              </div>
           </div>
         </div>
   </div>
 `;
 $("body").append(attachmentModalData);
 // 8. emit event new group created
 socket.emit("new-group-created", {groupChat: data.groupChat});
// 9.
// 10.
socket.emit("check-status");
$("body").append(data.membersModalData);
userTalk();
  }).fail(function (response) {
          alertify.notify(response.responseText, "error", 5);
        });
      });
    });
}
$(document).ready(function () {
  $("#input-search-friends-to-add-group-chat").bind(
    "keypress",
    callSearchFriends
  );
  $("#btn-search-friends-to-add-group-chat").bind("click", callSearchFriends);
  callCreateGroupChat();

  // handle socket data response
  socket.on("response-new-group-created", function(response){
            // 1. hide modal -> nothing
            // 2. handle left-side data
            let subGroupChatName = response.groupChat.name;
            if (subGroupChatName.length > 15) {
              subGroupChatName = subGroupChatName.substr(0, 13) + "...";
            }
            let leftSideData = `
    <a href="#uid_${response.groupChat._id}" class="room-chat" data-target="#to_${response.groupChat._id}">
                                <li class="person group-chat" data-chat="${response.groupChat._id}">
                                    <div class="left-avatar">
                                        <img src="images/users/group-avatar-trungquandev.png" alt="" />
                                    </div>
                                    <p class="name">
                                        <span class="group-chat-name">
                                            ${subGroupChatName}
                                        </span>
                                    </p>
                                    <span class="time">
                                   </span>
                                    <span class="preview convert-emoji">
                                    </span>
                                </li>
                            </a>
    `;
            $("#all-chat").find("ul").prepend(leftSideData);
            $("#group-chat").find("ul").prepend(leftSideData);
            // 3. handle right side
            let rightSideData = `
    <div class="right tab-pane" data-chat="${response.groupChat._id}" id="to_${response.groupChat._id}">
    <div class="top">
        <span>To: <span class="name">${response.groupChat.name}</span></span>
        <span class="chat-menu-right">
          <a href="#attachmentsModal_${response.groupChat._id}" class="show-attachments" data-toggle="modal">
          Attachment
            <i class="fa fa-paperclip"></i>
          </a>
        </span>
        <span class="chat-menu-right">
          <a href="javascript:void(0)">&nbsp;</a>
        </span>
        <span class="chat-menu-right">
          <a href="#imagesModal_${response.groupChat._id}" class="show-images" data-toggle="modal">
            Image
            <i class="fa fa-photo"></i>
          </a>
        </span>
        <span class="chat-menu-right">
          <a href="javascript:void(0)">&nbsp;</a>
        </span>
        <span class="chat-menu-right">
          <a href="#membersModal_${response.groupChat._id}" class="number-members" data-toggle="modal">
            <span class="show-number-members">${response.groupChat.userAmount}</span>

            <i class="fa fa-users"></i>
          </a>
        </span>
        <span class="chat-menu-right">
          <a href="javascript:void(0)">&nbsp;</a>
        </span>
        <span class="chat-menu-right">
          <a href="javascript:void(0)" class="number-messages" data-toggle="modal">
            <span class="show-number-messages">${response.groupChat.messageAmount}</span>

            <i class="fa fa-comment"></i>
          </a>
        </span>
      </div>
      <div class="content-chat">
        <div class="chat chat-in-group" data-chat="${response.groupChat._id}">
        </div>
      </div>
      <div class="write" data-chat="${response.groupChat._id}">
        <input type="text" id="write-chat-${response.groupChat._id}" class="write-chat chat-in-group" data-chat="${response.groupChat._id}" />
        <div class="icons">
          <a href="#" class="icon-chat" data-chat="${response.groupChat._id}"
            ><i class="fa fa-smile-o"></i
          ></a>
          <label for="image-chat-${response.groupChat._id}" class="image-chat-label">
            <input
              type="file"
              id="image-chat-${response.groupChat._id}"
              name="my-image-chat"
              class="image-chat chat-in-group"
              data-chat="${response.groupChat._id}"
            />
            <i class="fa fa-photo"></i>
          </label>
          <label for="attachment-chat-${response.groupChat._id}" class="attachment-chat-label">
            <input
              type="file"
              id="attachment-chat-${response.groupChat._id}"
              name="my-attachment-chat"
              class="attachment-chat chat-in-group"
              data-chat="${response.groupChat._id}"
            />
            <i class="fa fa-paperclip"></i>
          </label>
          <a
            href="#streamModal"
            id="video-chat-group"
          >
            <i class="fa fa-video-camera"></i>
          </a>
        </div>
      </div>
</div>
    `;
            $("#screen-chat").prepend(rightSideData);
            // 4. call change screen chat - js/mainConfig.js
            changeScreenChat();
            // 5. handle image modal
            let imageModalData = `
                <div class="modal fade" id="imagesModal_${response.groupChat._id}" role="dialog">
                    <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                      &times;
                    </button>
                    <h4 class="modal-title">All Images in this conversation</h4>
                  </div>
                  <div class="modal-body">
                    <div class="all-images" style="visibility: hidden;">
                    </div>
                  </div>
                </div>
              </div>
            </div>
    `;
            $("body").append(imageModalData);
            // 6. call  gridPhoto = js/mainConfig.js
            gridPhotos(5);
            // 7.handle attachment modal
            let attachmentModalData = `
          <div class="modal fade" id="attachmentsModal_${response.groupChat._id}" role="dialog">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">
             &times;
           </button>
           <h4 class="modal-title">All Attachments in this conversation.</h4>
             </div>
               <div class="modal-body">
                   <ul class="list-attachments">
                   </ul>
         <     /div>
           </div>
         </div>
   </div>
 `;
 $("body").append(attachmentModalData);

 //9.
  socket.emit("member-received-group-chat",{groupChatId: response.groupChat._id});
  $("body").append(response.membersModalData);
  });
  // 10. update online
  socket.emit("check-status");

   userTalk();
});
